package br.com.almana.webapi.exception.mappers;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;
import javax.validation.ValidationException;

/**
 * Created by francisco on 10/18/15.
 */
@Produces
@Singleton
@Requires(classes = {ValidationException.class, ExceptionHandler.class})
public class ValidationExceptionHandler implements ExceptionHandler<ValidationException, HttpResponse> {
    @Override
    public HttpResponse handle(HttpRequest request, ValidationException exception) {
        return HttpResponse.badRequest(String.format("{\"message\": \"%s\"}", exception.getMessage()));
    }
}
