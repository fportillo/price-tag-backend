package br.com.almana.webapi.exception.mappers;

import br.com.almana.exception.ShoppingListNotFoundException;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;


/**
 * Created by laercio on 07/11/15.
 */
@Produces
@Singleton
@Requires(classes = {ShoppingListNotFoundException.class, ExceptionHandler.class})
public class ShoppingListNotFoundExceptionMapper implements ExceptionHandler<ShoppingListNotFoundException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, ShoppingListNotFoundException exception) {
        return HttpResponse.notFound(ErrorMessageBodies.NOT_FOUND_MSG);
    }
}
