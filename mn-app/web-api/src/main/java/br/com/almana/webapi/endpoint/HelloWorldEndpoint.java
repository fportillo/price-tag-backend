package br.com.almana.webapi.endpoint;

import br.com.almana.service.SearchService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import org.bson.Document;

import java.util.List;

/**
 * REST endpoint for testing authentication.
 */
@Controller(HelloWorldEndpoint.PATH)
public class HelloWorldEndpoint {

    public static final String PATH = "/helloworld";

    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public String onlyRootsGetThis() {
        return "{\"message\":\"Hello World\"}";
    }

    // FIXME - this is just a map-reduce test. move this to a better place when appropriate
    @Get("/mapreduce")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Document> mapReduce() {
        return new SearchService().searchBestPrices(-22.8785233, -47.0464989, 30000, new String[]{"4005808806430", "7896098903032", "7891022100372", "7501006705928", "7891021002165", "7891080112836", "7893000439825"});
    }

}
