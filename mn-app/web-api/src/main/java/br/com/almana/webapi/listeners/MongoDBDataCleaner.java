package br.com.almana.webapi.listeners;

import br.com.almana.repository.Mongo;
import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.event.ApplicationShutdownEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

/**
 * Created by francisco on 10/10/15.
 */
@SuppressWarnings("unused")
@Singleton
public class MongoDBDataCleaner implements ApplicationEventListener<ApplicationShutdownEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBDataCleaner.class);

    @Override
    public void onApplicationEvent(ApplicationShutdownEvent event) {
        LOGGER.info("Closing mongo connections...");
        Mongo.INSTANCE.close();
        LOGGER.info("Mongo connections have been closed.");
    }
}
