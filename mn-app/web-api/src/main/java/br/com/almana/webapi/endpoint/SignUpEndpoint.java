package br.com.almana.webapi.endpoint;

import br.com.almana.domain.EmailSignUp;
import br.com.almana.service.SecurityService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;

import javax.inject.Inject;

/**
 * Class to hold sign up interface
 * Created by francisco on 2/7/16.
 */
@Controller("/sign-up")
public class SignUpEndpoint {

    @Inject
    @SuppressWarnings("unused")
    private SecurityService securityService;

    @Post("/email")
    public HttpResponse<?> emailSignUp(EmailSignUp emailSignUp) {
        securityService.signUp(emailSignUp);
        return HttpResponse.accepted();
    }

}
