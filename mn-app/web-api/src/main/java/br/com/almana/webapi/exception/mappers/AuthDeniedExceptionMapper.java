package br.com.almana.webapi.exception.mappers;

import br.com.almana.exception.AuthDeniedException;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;

/**
 * Mapper for invalid login attempts.
 * Created by francisco on 12/2/15.
 */
@Produces
@Singleton
@Requires(classes = {Throwable.class, ExceptionHandler.class})
public class AuthDeniedExceptionMapper implements ExceptionHandler<AuthDeniedException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, AuthDeniedException exception) {
        return HttpResponse.unauthorized();
    }
}
