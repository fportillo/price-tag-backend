package br.com.almana.webapi.filters;

import br.com.almana.config.AppConfig;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.micronaut.http.simple.SimpleHttpResponseFactory;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by francisco on 10/19/15.
 */
@SuppressWarnings("unused")
@Filter("/*")
public class RequestThrottleFilter implements HttpServerFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestThrottleFilter.class);

    private static final int MAX_REQUESTS_PER_SECOND = AppConfig.getInstance().getMaxRequestsPerSecond();
    private static final int PERIOD = 1000 / MAX_REQUESTS_PER_SECOND;
    private static final Map<String, Slot> REGISTRY = new ConcurrentHashMap<>();

    @Override
    public Publisher<MutableHttpResponse<?>> doFilter(HttpRequest<?> request, ServerFilterChain chain) {
        // TODO: writing blocking code now in netty is evil, need to refactor the below to a more
        // RxJava Flowable functional way.

        LOGGER.debug("Avoiding throttling");

        // filtering by IP for now
        Optional<String> maybeARemoteIp = Optional.of(request.getRemoteAddress())
                .map(InetSocketAddress::getAddress)
                .map(InetAddress::toString);

        if (maybeARemoteIp.isPresent()) {
            String remoteIp = maybeARemoteIp.get();
            final Slot slot = REGISTRY.computeIfAbsent(remoteIp, (i) -> new Slot());

            if (slot.isPast()) {
                LOGGER.debug("Slot for {} is past", maybeARemoteIp);
                REGISTRY.put(remoteIp, new Slot());
            } else if (slot.isFull()) {
                LOGGER.debug("Slot for {} is full", maybeARemoteIp);
                MutableHttpResponse<String> forbidden = SimpleHttpResponseFactory.INSTANCE.status(HttpStatus.FORBIDDEN, "{\"message\":\"Please, slow down!\"}");
                return Flowable.just(forbidden);
            } else {
                LOGGER.debug("Slot for {} is ok, taking one", maybeARemoteIp);
                slot.take();
            }

            if (REGISTRY.size() % 100 == 0) {
                CacheCleaner.removeExpiredEntries(REGISTRY);
            }
        }
        return chain.proceed(request);
    }

    public static class CacheCleaner {

        public static void removeExpiredEntries(Map<String, Slot> requestCache) {
            Set<String> expiredKeys = requestCache.entrySet().stream()
                    .filter((entry) -> entry.getValue().isExpired())
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
            expiredKeys.forEach(requestCache::remove);
        }
    }
}
