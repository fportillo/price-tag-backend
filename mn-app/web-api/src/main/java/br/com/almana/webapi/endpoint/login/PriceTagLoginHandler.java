package br.com.almana.webapi.endpoint.login;

import br.com.almana.domain.AuthToken;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.handlers.LoginHandler;

import javax.inject.Singleton;

@Singleton
public class PriceTagLoginHandler implements LoginHandler {
    @Override
    public MutableHttpResponse<?> loginSuccess(UserDetails userDetails, HttpRequest<?> request) {
        // TODO: this is just adapting to the micronaut security model. Previous model used to return AuthToken body
        // this is just passed in the UserDetails because this callback LoginHandler has fixed types.
        return HttpResponse.ok(userDetails.getAttributes("doesNot", "matter").get(AuthToken.MAP_KEY));
    }

    @Override
    public MutableHttpResponse<?> loginFailed(AuthenticationResponse authenticationResponse) {
        return HttpResponse.unauthorized();
    }
}
