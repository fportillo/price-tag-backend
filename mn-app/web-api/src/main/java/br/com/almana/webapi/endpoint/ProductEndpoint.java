package br.com.almana.webapi.endpoint;

import br.com.almana.domain.Product;
import br.com.almana.repository.Repository;
import br.com.almana.service.ProductService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import org.bson.types.ObjectId;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.validation.constraints.Null;
import java.net.URI;
import java.util.List;

/**
 * REST endpoint for prices
 */
@Controller(ProductEndpoint.PATH)
@Secured(SecurityRule.IS_ANONYMOUS) // TODO: fix authentication
public class ProductEndpoint {

    public static final String PATH = Product.API_PATH;

    private final ProductService productService;

    @Inject
    public ProductEndpoint(ProductService productService) {
        this.productService = productService;
    }

    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> priceSearch(@QueryValue("ean") String barCode,
                                     @QueryValue("lat") Double latitude,
                                     @QueryValue("long") Double longitude,
                                     @QueryValue("n") String name) {
        return productService.findByParams(barCode, latitude, longitude, name);
    }

    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<Product> addPrice(@Body Product product) {
        URI uri = productService.save(product);
        return HttpResponse.created(product, uri);
    }


    @Get("/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<Product> get(@PathVariable("uuid") String uuid) {
        // TODO: make a method find by id on productService. return productService.findById(uuid).orElse(notFound())
        return Repository.getInstance(Product.class).get(new ObjectId(uuid))
                .map(HttpResponse::ok)
                . orElse(HttpResponse.notFound());
    }

}
