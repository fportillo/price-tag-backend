package br.com.almana.webapi.security;

import br.com.almana.domain.AuthToken;
import br.com.almana.domain.SessionInfo;
import br.com.almana.exception.AuthDeniedException;
import br.com.almana.service.SecurityService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.AuthenticationFailed;
import io.micronaut.security.authentication.AuthenticationProvider;
import io.micronaut.security.authentication.AuthenticationRequest;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.authentication.UserDetails;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Singleton
public class PriceTagAuthProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(PriceTagAuthProvider.class);

    private final SecurityService securityService;

    @Inject
    public PriceTagAuthProvider(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public Publisher<AuthenticationResponse> authenticate(@Nullable HttpRequest<?> httpRequest, AuthenticationRequest<?, ?> authenticationRequest) {
        // Get the HTTP Authorization header from the request
        Optional<String> maybeAnAuthHeader = Optional.ofNullable(httpRequest)
                .map(HttpRequest::getHeaders)
                .map((var headers) -> headers.get(HttpHeaders.AUTHORIZATION));

        // Check if the HTTP Authorization header is present and formatted correctly
        if (maybeAnAuthHeader.isEmpty()) {
            return userPwdBodyLogin(httpRequest);
        }
        return bearerTokenLogin(maybeAnAuthHeader.get());
    }

    private Publisher<AuthenticationResponse> bearerTokenLogin(String authHeader) {
        if (!authHeader.startsWith("Bearer ")) {
            return Flowable.just(invalidToken());
        }

        // Extract the token from the HTTP Authorization header
        String token = authHeader.substring("Bearer".length()).trim();

        try {
            return userInfo(token);

        } catch (Exception e) {
            return Flowable.just(invalidToken());
        }
    }

    private Publisher<AuthenticationResponse> userPwdBodyLogin(@Nullable HttpRequest<?> httpRequest) {
        // if no authorization header, try to login with body
        Optional<ObjectNode> maybeABody = Optional.ofNullable(httpRequest)
                .map(HttpRequest::getBody)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map((var body) -> ((ObjectNode)body));

        if (maybeABody.isEmpty()) {
            return Flowable.just(invalidToken());
        }

        ObjectNode body = maybeABody.get();

        try {
            AuthToken authToken = securityService.authenticate(body.get("username").asText(), body.get("password").asText());
            return userInfo(authToken.getToken());
        } catch (AuthDeniedException e) {
            return Flowable.just(invalidToken());
        }
    }

    private Publisher<AuthenticationResponse> userInfo(String token) {
        // Validate the token
        SessionInfo user = securityService.authenticate(token);

        final Map<String, Object> tokenMap = Map.of(AuthToken.MAP_KEY, user.getAuthToken());

        // if token valid
        return Flowable.just(new UserDetails(Objects.requireNonNull(user.getUserEmail()), Arrays.asList(user.getUserRoles()), tokenMap));
    }

    private AuthenticationFailed invalidToken() {
        return new AuthenticationFailed("Invalid token.");
    }
}
