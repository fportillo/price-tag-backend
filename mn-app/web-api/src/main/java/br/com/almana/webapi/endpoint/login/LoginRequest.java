package br.com.almana.webapi.endpoint.login;

/**
 * Pojo for login requests
 * Created by francisco on 11/30/15.
 */
public class LoginRequest {

    private String username;
    private String password;

    public LoginRequest() {
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
