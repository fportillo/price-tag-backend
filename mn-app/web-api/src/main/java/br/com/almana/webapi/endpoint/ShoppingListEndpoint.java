package br.com.almana.webapi.endpoint;

import br.com.almana.domain.ShoppingList;
import br.com.almana.service.UserService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;

import javax.inject.Inject;
import java.net.URI;
import java.util.List;

/**
 * REST endpoint for ShoppingList endpoint.
 * Created by francisco on 10/28/15.
 */
@Controller("/user/{useruuid}/shopping-list")
public class ShoppingListEndpoint {

    @SuppressWarnings("unused")
    private final UserService service;

    @Inject
    public ShoppingListEndpoint(UserService service) {
        this.service = service;
    }


    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<ShoppingList> createNewShoppingList(@Body ShoppingList shoppingList,
                                                            @PathVariable("useruuid") String userId) {

        final URI uri = service.saveShoppingList(shoppingList, userId);
        return HttpResponse.created(shoppingList, uri);
    }

    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<List<ShoppingList>> getShoppingLists(@PathVariable("useruuid") String userId) {
        return HttpResponse.ok(service.getShoppingLists(userId));
    }

    @Put
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<ShoppingList> updateShoppingList(@Body ShoppingList shoppingList,
                                       @PathVariable("useruuid") String userId) {
        return HttpResponse.ok(service.updateShoppingList(userId, shoppingList));
    }

    @Delete("/{slistId}")
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<ShoppingList> deleteShoppingList(
            @PathVariable("useruuid") String userId,
            @PathVariable("slistId") String shoppingListId) {
        final ShoppingList shoppingList = service.deleteShoppingList(userId, shoppingListId);
        return HttpResponse.ok(shoppingList);
    }

    @Get("/{slistId}")
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<ShoppingList> getShoppingList(
            @PathVariable("useruuid") String userId,
            @PathVariable("slistId") String shoppingListId) {
        final ShoppingList shoppingList = service.getShoppingList(userId, shoppingListId);
        return HttpResponse.ok(shoppingList);
    }

}
