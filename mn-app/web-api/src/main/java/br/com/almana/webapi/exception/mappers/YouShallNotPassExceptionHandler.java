package br.com.almana.webapi.exception.mappers;

import br.com.almana.exception.YouShallNotPassException;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;

/**
 * Exception mapper for forbidden actions.
 * Created by francisco on 12/3/15.
 */
@Produces
@Singleton
@Requires(classes = {YouShallNotPassException.class, ExceptionHandler.class})
public class YouShallNotPassExceptionHandler implements ExceptionHandler<YouShallNotPassException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, YouShallNotPassException exception) {
        String message = String.format("{\"message\":\"%s\"}", exception.getMessage());
        return HttpResponse.status(HttpStatus.FORBIDDEN).body(message);
    }
}
