package br.com.almana.webapi.endpoint;

import br.com.almana.service.SecurityService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.RequestAttribute;

import javax.inject.Inject;

/**
 * Endpoint class for verifying user emails.
 * Created by francisco on 2/8/16.
 */
@Controller("/verify")
public class EmailVerificationEndpoint {

    @Inject
    @SuppressWarnings("unused")
    private SecurityService securityService;

    @Get
    public String verify(@RequestAttribute("ct") String ct) {

        securityService.verifyEmail(ct);
        // TODO: suggestion: try to make this generic to 'forgot password' feature.

        return "{\"TODO\":\"IMPLEMENT\"}";
    }

}
