package br.com.almana.webapi.endpoint.search;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by francisco on 3/8/16.
 */
public class ShoppingListSearchRequest {

    @NotEmpty
    private String[] barCodes;

    @NotNull
    private Integer maxDistance;

    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;

    public String[] getBarCodes() {
        return barCodes;
    }

    public void setBarCodes(String[] barCodes) {
        this.barCodes = barCodes;
    }

    public Integer getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(Integer maxDistance) {
        this.maxDistance = maxDistance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
