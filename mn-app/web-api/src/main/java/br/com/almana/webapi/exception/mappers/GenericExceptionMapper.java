package br.com.almana.webapi.exception.mappers;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

/**
 * Created by francisco on 10/18/15.
 */
@Produces
@Singleton
@Requires(classes = {Throwable.class, ExceptionHandler.class})
public class GenericExceptionMapper implements ExceptionHandler<Throwable, HttpResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericExceptionMapper.class);

    @Override
    public HttpResponse handle(HttpRequest request, Throwable exception) {
        LOGGER.error("An unexpected error happened: ", exception);
        return HttpResponse.serverError(ErrorMessageBodies.OOPS);
    }
}
