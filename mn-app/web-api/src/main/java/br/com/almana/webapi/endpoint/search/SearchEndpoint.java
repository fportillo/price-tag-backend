package br.com.almana.webapi.endpoint.search;

import br.com.almana.service.SearchService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by francisco on 3/8/16.
 */
@Controller(SearchEndpoint.PATH)
public class SearchEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchEndpoint.class);

    public static final String PATH = "/search";

    @Inject
    private SearchService searchService;

    @Post
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Document> searchCheapestShoppingListWithinRange(@Body ShoppingListSearchRequest shoppingListSearchRequest) {
        return searchService.searchBestPrices(shoppingListSearchRequest.getLatitude(),
                shoppingListSearchRequest.getLongitude(),
                shoppingListSearchRequest.getMaxDistance(),
                shoppingListSearchRequest.getBarCodes());
    }
}
