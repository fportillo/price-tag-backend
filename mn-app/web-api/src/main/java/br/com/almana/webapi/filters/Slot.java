package br.com.almana.webapi.filters;

import br.com.almana.config.AppConfig;

import java.time.Instant;
import java.util.Date;

/**
 * Helper class to hold slot of requests for a client.
 * Created by francisco on 10/20/15.
 */
public class Slot {

    private final static int EXPIRY_TIME_SECS = AppConfig.getInstance().getThrottleCacheExpireTimeInSecs();

    private final int maxSize;
    private final int period;
    private volatile int capacity;
    private final long startTime;

    public Slot() {
        this.maxSize = AppConfig.getInstance().getMaxRequestsPerSecond();
        this.capacity = 0;
        this.startTime = System.currentTimeMillis();
        this.period = 1000 / maxSize;
    }

    public boolean isPast() {
        long now = System.currentTimeMillis();
        return now > (getStartTime() + period);
    }

    public void take() {
        capacity++;
    }

    public boolean isFull() {
        return capacity >= maxSize;
    }

    public boolean isExpired() {
        return Date.from(Instant.now().minusSeconds(EXPIRY_TIME_SECS)).getTime() >= getStartTime();
    }

    protected long getStartTime() {
        return startTime;
    }
}
