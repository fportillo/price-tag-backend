package br.com.almana.webapi.listeners;

import br.com.almana.util.DataLoader;
import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.event.ApplicationStartupEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

/**
 * Created by francisco on 10/10/15.
 */
@SuppressWarnings("unused")
@Singleton
public class MongoDBDataLoader implements ApplicationEventListener<ApplicationStartupEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBDataLoader.class);

    @Override
    public void onApplicationEvent(ApplicationStartupEvent event) {
        LOGGER.info("Testing mongo connection");
        if (StringUtils.isNotBlank(System.getProperty("devDataLoad"))) {
            LOGGER.info("Loading fresh development data, as configured...");
            DataLoader.cleanup();
            DataLoader.load();
        }
    }

}
