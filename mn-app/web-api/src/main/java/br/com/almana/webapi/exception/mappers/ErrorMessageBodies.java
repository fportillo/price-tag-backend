package br.com.almana.webapi.exception.mappers;

/**
 * Created by laercio on 07/11/15.
 */
public class ErrorMessageBodies {
    public static final String NOT_FOUND_MSG = "{\"message\":\"We could not find what you requested.\"}";
    public static final String OOPS = "{\"message\": \"Oops, something unexpected happened.\"}";
}
