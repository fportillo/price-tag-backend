package br.com.almana.webapi.filters;

import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;

public class RequestThrottleFilterTest {

    @Test
    public void testCacheCleaner() {
        Map<String, Slot> map = new ConcurrentHashMap<>();

        final Slot fooSlot = new Slot();
        map.put("foo", fooSlot);

        // bar is expired and is going to be removed
        map.put("bar", new Slot() {
            @Override
            public boolean isExpired() {
                return true;
            }
        });

        RequestThrottleFilter.CacheCleaner.removeExpiredEntries(map);

        assertEquals(1, map.size());
        assertEquals(map.get("foo"), fooSlot);
    }
}
