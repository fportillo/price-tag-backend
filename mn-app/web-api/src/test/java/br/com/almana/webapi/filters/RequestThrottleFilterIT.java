package br.com.almana.webapi.filters;

import br.com.almana.config.AppConfig;
import br.com.almana.webapi.endpoint.BaseWebIT;
import io.micronaut.http.HttpResponse;
import org.junit.After;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RequestThrottleFilterIT extends BaseWebIT {

    private static final int ORIGINAL_MAX_REQUESTS_PER_SECOND = AppConfig.getInstance().getMaxRequestsPerSecond();

    @Test
    public void testAvoidThrottling() {
        AppConfig.getInstance().setMaxRequestsPerSecond(2);
        int maxRequests = AppConfig.getInstance().getMaxRequestsPerSecond();

        HttpResponse<String> response = null;
        for (int i = 0; i < maxRequests * 2; i++) {
            response = getRequest("/health", String.class);
        }

        assertNotNull(response);
        assertEquals("{\"message\":\"Please, slow down!\"}", response.body());
    }

    @After
    public void tearDown() {
        AppConfig.getInstance().setMaxRequestsPerSecond(ORIGINAL_MAX_REQUESTS_PER_SECOND);
    }

}
