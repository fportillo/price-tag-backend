package br.com.almana.webapi.endpoint;

import br.com.almana.domain.AuthToken;
import br.com.almana.domain.Role;
import br.com.almana.domain.User;
import br.com.almana.repository.Repository;
import br.com.almana.util.fixture.UserFixture;
import br.com.almana.webapi.endpoint.login.LoginRequest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.simple.SimpleHttpRequest;
import org.junit.After;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SecurityIT extends BaseWebIT {

    private final Repository<User> userRepository;
    private User john;

    public SecurityIT() {
        this.userRepository = Repository.getInstance(User.class);
    }

    @Test
    public void testSecurity() throws Exception {

        // cannot access product api without being logged in
        HttpResponse<String> response = getRequest(ProductEndpoint.PATH, String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatus());
        assertEquals("{\"message\":\"Authorization header must be provided.\"}", response.body());

        // invalid Authentication token results unauthorized again
        MutableHttpRequest simpleHttpRequest = methodGetRequest(ProductEndpoint.PATH, Collections.emptyMap());
        simpleHttpRequest.header("Authorization", "Bearer invalidToken");
        response = exchange(simpleHttpRequest, String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatus());
        assertEquals("{\"message\":\"Invalid token.\"}", response.body());

        // lets log in John
        final LoginRequest loginRequest = new LoginRequest();
        john = UserFixture.JOHN_DOE.getUser();
        loginRequest.setUsername(john.getEmail());
        loginRequest.setPassword("foobar");
        HttpResponse<AuthToken> authenticatedResponse = postRequest("/login", loginRequest, AuthToken.class);
        okResponse(response);

        AuthToken authToken = authenticatedResponse.body();
        assertNotNull(authToken.get_id());
        assertNotNull(authToken.getUser_id());
        assertNotNull(authToken.getToken());
        assertNotNull(authToken.getCreatedAt());
        assertNotNull(authToken.getExpiresAt());

        // now John should be able to grab endpoint
        simpleHttpRequest = methodGetRequest(ProductEndpoint.PATH, Collections.emptyMap());
        simpleHttpRequest.header("Authorization", "Bearer " + authToken.getToken());
        response = exchange(simpleHttpRequest, String.class);
        okResponse(response);

        // but if we update Johns roles to something else, 403 forbidden now
        // FIXME TODO role update method should be in SecurityService.
        john.setRoles(Collections.singletonList(Role.NOBODY_SHOULD_HAVE_THIS_ROLE));
        userRepository.replace(john);

        simpleHttpRequest = methodGetRequest(ProductEndpoint.PATH, Collections.emptyMap());
        simpleHttpRequest.header("Authorization", "Bearer " + authToken.getToken());
        response = exchange(simpleHttpRequest, String.class);
        assertEquals(403, response.getStatus().getCode());
        assertEquals("{\"message\":\"john.doe@gmail.com does not have the necessary roles.\"}", response.body());
    }

    @After
    public void tearDown() {
        john.setRoles(Collections.singletonList(Role.USER));
        userRepository.replace(john);
    }


}