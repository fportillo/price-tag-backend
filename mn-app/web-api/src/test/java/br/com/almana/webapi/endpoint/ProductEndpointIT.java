package br.com.almana.webapi.endpoint;

import br.com.almana.domain.Product;
import br.com.almana.domain.Shop;
import br.com.almana.repository.Mongo;
import br.com.almana.util.StringNormalizer;
import br.com.almana.util.fixture.ProductFixture;
import br.com.almana.util.fixture.ShopFixture;
import io.micronaut.http.HttpResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Disabled("TODO: fix ITs, still WIP")
public class ProductEndpointIT extends BaseWebIT {

    @Test
    @Disabled("TODO: fix ITs, still WIP")
    public void testAddPrice() {


        // test add price without normalized name and complete shop
        Product product = ProductFixture.DIET_WAY.getProduct();
        product.setNormalizedName(null);
        product.set_id(null);

        HttpResponse<Product> response = postRequest(ProductEndpoint.PATH, product, Product.class);

        createdResponse(response);
        Product productReturned = response.body();
        assertEquals(product.getName(), productReturned.getName());
        assertEquals(StringNormalizer.removeAccents(product.getName()), productReturned.getNormalizedName());
        assertEquals(product.getPrice(), productReturned.getPrice());
        assertEquals(product.getBarCode(), productReturned.getBarCode());

        // test add price without shop
        testWithShop(null);

        // test add price with only shop name
        Shop onlyWithName = Shop.newInstance(0.00, 0.00, "Whatever shop");
        testWithShop(onlyWithName);

        // test add price with only shop location
        testWithShop(Shop.newInstance(ShopFixture.DALBEN_TAQUARAL.getShop().getLocation().getLatitude(), ShopFixture.DALBEN_TAQUARAL.getShop().getLocation().getLongitude(), null));

    }

    private void testWithShop(final Shop shop) {
        Product product = ProductFixture.DIET_WAY.getProduct();
        product.setShop(shop);
        HttpResponse<Product> response = postRequest(ProductEndpoint.PATH, product, Product.class);
        createdResponse(response);
    }

    @Test
    @Disabled("TODO: fix ITs, still WIP")
    public void testGePriceByBarCode() throws IOException {
        Product product = ProductFixture.ANA_MARIA_CHOCOLATE.getProduct();

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("ean", product.getBarCode());
        HttpResponse<Product[]> response = getRequest(ProductEndpoint.PATH, queryParams, Product[].class);

        okResponse(response);
        Product[] productsReturned = response.body();

        assertEquals(product.getName(), productsReturned[0].getName());
        assertEquals(product.get_id(), productsReturned[0].get_id());
        assertEquals(product.getPrice(), productsReturned[0].getPrice());
        assertEquals(product.getDateCreated().toString(), productsReturned[0].getDateCreated().toString());
        assertEquals(product.getLastUpdate().toString(), productsReturned[0].getLastUpdate().toString());
        assertEquals(product.getThumbnailUrl(), productsReturned[0].getThumbnailUrl());
        assertEquals(product.isVerified(), productsReturned[0].isVerified());
    }

    @Test
    @Disabled("TODO: fix ITs, still WIP")
    public void testGetPriceByNamePart() {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("n", "çÃo");
        HttpResponse<String[]> response = getRequest(ProductEndpoint.PATH, queryParams, String[].class);

        okResponse(response);
        String[] jsonList = response.body();
        assertTrue(jsonList.length > 0);
    }

    @AfterAll
    public static void tearDownClass() {
        Mongo.INSTANCE.productCollection().drop();
    }


}
