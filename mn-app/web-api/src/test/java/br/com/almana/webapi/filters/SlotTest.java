package br.com.almana.webapi.filters;

import br.com.almana.config.AppConfig;
import org.junit.Test;

import java.sql.Date;
import java.time.Instant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SlotTest {

    @Test
    public void testMaxCapacity() {
        Slot slot = new Slot();
        int maxRequests = AppConfig.getInstance().getMaxRequestsPerSecond();
        for (int i = 0; i < maxRequests; i++) {
            slot.take();
        }
        assertTrue(slot.isFull());
    }

    @Test
    public void testIsPast() throws InterruptedException {
        int period = 1000 / AppConfig.getInstance().getMaxRequestsPerSecond();
        Slot past = new Slot() {
            @Override
            protected long getStartTime() {
                return period;
            }
        };
        assertTrue(past.isPast());

        Slot notPast = new Slot();
        assertFalse(notPast.isPast());
        assertFalse(notPast.isPast());
    }

    @Test
    public void testIsExpired() {
        Slot brandNew = new Slot();
        assertFalse(brandNew.isExpired());

        Slot expired = new Slot() {
            @Override
            protected long getStartTime() {
                return Date.from(Instant.now().minusSeconds(AppConfig.getInstance().getThrottleCacheExpireTimeInSecs())).getTime();
            }
        };

        assertTrue(expired.isExpired());

    }


}