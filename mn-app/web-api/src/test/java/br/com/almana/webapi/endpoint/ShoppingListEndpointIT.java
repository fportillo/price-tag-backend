package br.com.almana.webapi.endpoint;

import br.com.almana.domain.ShoppingList;
import br.com.almana.domain.User;
import br.com.almana.repository.Repository;
import br.com.almana.util.fixture.ProductFixture;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import org.bson.types.ObjectId;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * API test for shopping list operations
 * Created by francisco on 11/2/15.
 */
public class ShoppingListEndpointIT extends BaseWebIT {


    private final Repository<User> repository;
    private final User user;

    public ShoppingListEndpointIT() {
        this.repository = Repository.getInstance(User.class);
        this.user = repository.findFirst();
    }

    @Test
    public void testCreateNewShoppingList() throws IOException {

        String userId = user.get_id();

        createShoppingList(userId, "My Test ShoppingList");
    }

    private void createShoppingList(String userId, String shoppingListName) throws IOException {
        ShoppingList shoppingList = ShoppingList.newInstance(shoppingListName, Optional.empty());
        HttpResponse<ShoppingList> response = postRequest(getShoppingListRootUri(userId), shoppingList, ShoppingList.class);

        assertEquals(201, response.getStatus().getCode());

        ShoppingList created = response.getBody().get();

        assertEquals(shoppingListName, created.getName());
        assertEquals(shoppingList.getUuid(), created.getUuid());
        assertTrue(shoppingList.getItems().isEmpty());
    }

    private String getShoppingListRootUri(String userId) {
        return "/user/" + userId + "/shopping-list";
    }

    @Test
    public void testGetOneShoppingList() throws IOException {
        createShoppingList(user.get_id(), "My First Shopping List");
        createShoppingList(user.get_id(), "My Second Shopping List");

        // refresh user
        User refreshed = repository.get(new ObjectId(user.get_id())).get();

        final Iterator<ShoppingList> iterator = refreshed.getShoppingLists().iterator();
        while (iterator.hasNext()) {
            assertShoppingListIsExpected(iterator.next());
        }

    }

    @Test
    public void testGetUserShoppingLists() throws IOException {
        testGetOneShoppingList();

        HttpResponse<ShoppingList[]> exchange = getRequest(getUserShoppingListsUri(user.get_id()), ShoppingList[].class);
        ShoppingList[] usersShoppingLists = exchange.body();
        assertTrue(usersShoppingLists.length > 1);
    }

    @Test
    public void testUpdateShoppingList() throws IOException {
        testGetOneShoppingList();

        HttpResponse<ShoppingList[]> exchange = getRequest(getUserShoppingListsUri(user.get_id()), ShoppingList[].class);
        ShoppingList[] usersShoppingLists = exchange.body();
        assertTrue(usersShoppingLists.length > 1);

        final ShoppingList shoppingList = usersShoppingLists[0];
        shoppingList.addItem(ProductFixture.AMACIENTE_IPE_INTENSO_DALBEN.getProduct().asItem());
        shoppingList.addItem(ProductFixture.DETERGENTE_LIMPOL_CRISTAL_DALBEN.getProduct().asItem());
        shoppingList.addItem(ProductFixture.FILTRO_PAPEL_JOVITA_DALBEN.getProduct().asItem());

        HttpResponse<ShoppingList> response = putRequest(getUserShoppingListsUri(user.get_id()), shoppingList, ShoppingList.class);

        assertEquals(HttpStatus.OK, response.getStatus());
        assertShoppingListIsExpected(shoppingList);
    }

    @Test
    public void testDeleteShoppingList() throws IOException {
        testGetOneShoppingList();

        HttpResponse<ShoppingList[]> response = getRequest(getUserShoppingListsUri(user.get_id()), ShoppingList[].class);
        ShoppingList[] usersShoppingLists = response.body();

        assertTrue(usersShoppingLists.length > 1);

        ShoppingList toBeDeleted = usersShoppingLists[0];

        HttpResponse<ShoppingList> deleteResponse = deleteRequest(getSpecificShoppingListUri(user.get_id(), toBeDeleted.getUuid()), ShoppingList.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatus());

        HttpResponse<ShoppingList[]> latestResponse = getRequest(getUserShoppingListsUri(user.get_id()), ShoppingList[].class);
        ShoppingList[] updatedShoppingLists = latestResponse.body();

        assertEquals(usersShoppingLists.length - 1, updatedShoppingLists.length);
    }

    private String getUserShoppingListsUri(String uuid) {
        return getShoppingListRootUri(uuid);
    }

    private void assertShoppingListIsExpected(ShoppingList shoppingList) throws IOException {
        HttpResponse<ShoppingList> response = getRequest(getSpecificShoppingListUri(user.get_id(), shoppingList.getUuid()), ShoppingList.class);
        final ShoppingList returned = response.body();
        assertEquals(shoppingList, returned);
    }

    private String getSpecificShoppingListUri(String userId, UUID uuid) {
        return getShoppingListRootUri(userId) + "/" + uuid.toString();
    }

}
