package br.com.almana.webapi.endpoint;

import br.com.almana.EmailWorker;
import br.com.almana.domain.AuthToken;
import br.com.almana.domain.Product;
import br.com.almana.util.DataLoader;
import br.com.almana.util.fixture.UserFixture;
import br.com.almana.webapi.endpoint.login.LoginRequest;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import io.micronaut.context.ApplicationContext;
import io.micronaut.http.HttpMethod;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpRequestFactory;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public abstract class BaseWebIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseWebIT.class);
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    protected static final GreenMail GREEN_MAIL = new GreenMail(ServerSetupTest.SMTP);

    private static EmbeddedServer embeddedServer;
    protected static HttpClient client;
    protected static AuthToken token;

    private static void setupServer() {
        embeddedServer = ApplicationContext.run(EmbeddedServer.class);
        client = embeddedServer
                .getApplicationContext()
                .createBean(HttpClient.class, embeddedServer.getURL());
    }

    private static void login() {
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(UserFixture.JOHN_DOE.getUser().getEmail());
        loginRequest.setPassword("foobar");

        token = client.toBlocking().retrieve(HttpRequest.POST("/login", loginRequest), AuthToken.class);
    }

    @BeforeAll
    public static void beforeAll() throws Exception {
        setupServer();

        DataLoader.cleanup();
        LOGGER.info("Bootstrapping for endpoint tests...");
        DataLoader.load();

        // starts a fake local smtp server
        GREEN_MAIL.start();
        EmailWorker.start();

        login();
    }

    protected void createdResponse(HttpResponse<Product> response) {
        assertEquals(HttpStatus.CREATED, response.getStatus());
    }

    @AfterAll
    public static void afterAll() throws Exception {
        DataLoader.cleanup();
        GREEN_MAIL.stop();
        EmailWorker.stop();
        stopServer();
    }

    private static void stopServer() {
        if (embeddedServer != null) {
            embeddedServer.stop();
        }
        if (client != null) {
            client.stop();
        }
    }

    protected <T> HttpRequest<T> request(String path, HttpMethod method, T body) {
        MutableHttpRequest<T> tSimpleHttpRequest = HttpRequestFactory.INSTANCE.create(method, path);
        tSimpleHttpRequest.body(body);
        tSimpleHttpRequest.bearerAuth(token.getToken());
        return tSimpleHttpRequest;
    }

    protected <O> HttpResponse<O> getRequest(String path, Map<String, String> queryParams, Class<O> clazz) {
        return exchange(methodGetRequest(path, queryParams), clazz);
    }

    protected <O> HttpResponse<O> getRequest(String path, Class<O> clazz) {
        return exchange(methodGetRequest(path, Collections.emptyMap()), clazz);
    }

    protected <T, O> HttpResponse<O> postRequest(String path, T body, Class<O> clazz) {
        return exchange(path, body, clazz, HttpMethod.POST);
    }

    protected <T, O> HttpResponse<O> putRequest(String path, T body, Class<O> clazz) {
        return exchange(path, body, clazz, HttpMethod.PUT);
    }

    protected <T, O> HttpResponse<O> deleteRequest(String path, Class<O> clazz) {
        return exchange(path, null, clazz, HttpMethod.DELETE);
    }

    private <T, O> HttpResponse<O> exchange(String path, T body, Class<O> clazz, HttpMethod method) {
        return exchange(request(path, method, body), clazz);
    }

    protected <O, T> HttpResponse<O> exchange(HttpRequest<T> request, Class<O> clazz) {
        return client.toBlocking().exchange(request, clazz);
    }

    @SuppressWarnings("rawtypes")
    protected MutableHttpRequest<Object> methodGetRequest(String path, Map<String, String> queryParams) {
        MutableHttpRequest<Object> mutableHttpRequest = HttpRequest.GET(path);
        mutableHttpRequest.bearerAuth(token.getToken());
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            mutableHttpRequest.getParameters().add(entry.getKey(), entry.getValue());
        }
        return mutableHttpRequest;
    }

    protected void okResponse(HttpResponse<?> response) {
        assertEquals(HttpStatus.OK, response.getStatus());
    }
}
