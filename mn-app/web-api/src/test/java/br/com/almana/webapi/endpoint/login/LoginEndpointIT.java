package br.com.almana.webapi.endpoint.login;

import br.com.almana.domain.User;
import br.com.almana.util.fixture.UserFixture;
import br.com.almana.webapi.endpoint.BaseWebIT;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * Created by francisco on 3/8/16.
 */
@Disabled("TODO: fix ITs, still WIP")
public class LoginEndpointIT extends BaseWebIT {

    @Test
    @Disabled("TODO: fix login migration")
    public void testLoginFailure() {
        LoginRequest request = new LoginRequest();
        request.setUsername("unexistent@gmail.com");
        request.setPassword("unmatchingPasswordYeah");
        Assertions.assertThrows(HttpClientResponseException.class, () -> client.toBlocking().exchange(HttpRequest.POST("/login", request)));
    }

    @Test
    @Disabled("TODO: fix login migration")
    public void testLoginSuccess() {
        LoginRequest request = new LoginRequest();
        User user = UserFixture.JOHN_DOE.getUser();
        request.setUsername(user.getEmail());
        request.setPassword("foobar");
        HttpResponse<String> response = client.toBlocking().exchange(HttpRequest.POST("/login", request));
        okResponse(response);
    }
}