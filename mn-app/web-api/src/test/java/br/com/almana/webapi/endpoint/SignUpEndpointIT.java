package br.com.almana.webapi.endpoint;

import br.com.almana.domain.EmailSignUp;
import br.com.almana.domain.User;
import br.com.almana.repository.Repository;
import br.com.almana.service.SecurityService;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.mongodb.BasicDBObject;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by francisco on 2/8/16.
 */
public class SignUpEndpointIT extends BaseWebIT {

    private final Repository<EmailSignUp> emailSignUpRepository;
    private final Repository<User> userRepository;
    private final SecurityService securityService;

    public SignUpEndpointIT() {
        emailSignUpRepository = Repository.getInstance(EmailSignUp.class);
        userRepository = Repository.getInstance(User.class);
        securityService = new SecurityService();
    }

    @Test
    public void testEmailSignUp() {
        final String email = "foo@bar";
        final String password = "12345678";
        final String name = "foo bar";

        EmailSignUp signUp = new EmailSignUp(email, password);
        signUp.setName(name);

        HttpResponse<String> response = postRequest("/sign-up/email", signUp, String.class);

        // assert response is expected.
        assertEquals(HttpStatus.ACCEPTED, response.getStatus());
        BasicDBObject query = new BasicDBObject("email", signUp.getEmail());

        final Optional<EmailSignUp> existing = emailSignUpRepository.findOneByQuery(query);
        final EmailSignUp persistedSignUp = existing.get();

        final Optional<User> maybeUser = userRepository.findOneByQuery(new BasicDBObject("email", signUp.getEmail()));
        final User user = maybeUser.get();

        assertEquals(email, persistedSignUp.getEmail());

        assertEquals(user.getPasswordHash(), persistedSignUp.getPassword());
        assertEquals(name, persistedSignUp.getName());
        final String confirmationToken = persistedSignUp.getConfirmationToken();
        assertNotNull(confirmationToken);
        final String confirmationLink = persistedSignUp.getConfirmationLink();
        assertNotNull(confirmationLink);

        // make sure email is received.
        assertTrue(GREEN_MAIL.waitForIncomingEmail(5000, 1));

        // make sure email is correct and contains proper link.
        final MimeMessage[] receivedMessages = GREEN_MAIL.getReceivedMessages();
        final String actualEmailBody = GreenMailUtil.getBody(receivedMessages[0]).trim();
        assertTrue(actualEmailBody.contains(confirmationLink));

        // verify the email
        HttpResponse<String> verificationResponse = getRequest(String.format("/verify", confirmationToken), Map.of("ct", confirmationToken), String.class);
        okResponse(verificationResponse);

        final EmailSignUp burnedSignUp = emailSignUpRepository.findOneByQuery(new BasicDBObject("email", signUp.getEmail())).get();

        // assert user is in the expected state
        assertEquals(User.Status.EMAIL_VERIFIED, userRepository.findOneByQuery(new BasicDBObject("email", signUp.getEmail())).get().getStatus());

        // assert that signUp is burned
        assertEquals(EmailSignUp.Status.BURNED, burnedSignUp.getStatus());

        // assert that authentication is possible
        securityService.authenticate(email, password);
    }

}