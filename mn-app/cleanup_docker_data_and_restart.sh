#!/bin/bash

rm -rf ./data/mongo/tmp/*
rm -rf ./data/rabbitmq/tmp/*

docker-compose rm -f
docker-compose up --remove-orphans
