// create location field to be indexed
// db.getCollection('price').find().forEach(
//    function(e) {
//        if (e.shop != null && e.shop.longitude != null && e.shop.latitude != null) {
//            e.location = { type: "Point", coordinates: [e.shop.longitude, e.shop.latitude] };
//        }
//        db.getCollection('price').save(e);
//    }
//);

// create location index
// db.price.createIndex({ location: "2dsphere" })

// geo-query for locations -22.8620675,-47.0407714 -> casa do laercio
db.product.find({
    $query: {
        location: {
            $near: {
                $geometry: {
                    type: "Point",
                    coordinates: [
                        -47.0464989,
                        -22.8785233
                    ]
                },
                $maxDistance: 3000
            }
        },
        barCode: { $in : ["4005808806430", "7896098903032", "7891022100372", "7501006705928", "7891021002165", "7891080112836", "7893000439825"]}
    },
    $orderby: {
        value: 1
    }
})

var shopListMapFunction = function() {
    emit(this.shop.name, {"name": this.name, "barCode": this.barCode, "value": this.price});
};

var shopListReduceFunction = function(shopName, products) {
    var values = products.map(function(e) { return e.value; });
    return {
        "name": shopName,
        "user": "Chico",
        "products": products,
        "sum": Array.sum(values)
    };
};

db.product.mapReduce(
    shopListMapFunction,
    shopListReduceFunction,
    {
        query: {
            location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [
                            -47.0464989,
                            -22.8785233
                        ]
                    },
                    $maxDistance: 30000
                }
            },
            barCode: { $in : ["4005808806430", "7896098903032", "7891022100372", "7501006705928", "7891021002165", "7891080112836", "7893000439825"]}
        },
        out: { inline: 1} }
);

//db.my_first_mongodb_map_reduce.find({$query: {}, $orderby : { sum: 1 }})
