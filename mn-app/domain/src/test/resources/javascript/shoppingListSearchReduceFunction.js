function(shopName, products) {
    var values = products.map(function(e) { return e.value; });
    return {
        "products": products,
        "sum": Array.sum(values)
    };
};
