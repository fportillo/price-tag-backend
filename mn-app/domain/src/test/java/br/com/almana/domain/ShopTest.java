package br.com.almana.domain;

import br.com.almana.util.fixture.ShopFixture;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by francisco on 10/24/15.
 */
public class ShopTest {

    @Test
    public void testLatitudeAndLongitudeDecimalPrecision() {
        Shop dalben = ShopFixture.DALBEN_TAQUARAL.getShop();
        assertEquals(-22.8785233, dalben.getLocation().getLatitude(), 0.0000000);
        assertEquals(-47.0464989, dalben.getLocation().getLongitude(), 0.0000000);
        assertEquals("Dalben Taquaral", dalben.getName());
    }
}