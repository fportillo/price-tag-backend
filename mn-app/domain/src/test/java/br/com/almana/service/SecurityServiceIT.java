package br.com.almana.service;

import br.com.almana.domain.AuthToken;
import br.com.almana.domain.SessionInfo;
import br.com.almana.domain.User;
import br.com.almana.exception.AuthDeniedException;
import br.com.almana.repository.Repository;
import br.com.almana.util.fixture.UserFixture;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.mongodb.BasicDBObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SecurityServiceIT extends BaseIT {

    private final SecurityService securityService;
    private final Repository<User> userRepo;
    private final User johnDoe;
    private AuthToken token;
    private static HazelcastInstance hazelcastInstance;
    private static Map<String, SessionInfo> cache;
    private final int sessionDurationInMonths = 12;

    public SecurityServiceIT() {
        // let's just mock the session eviction/ttl info from this class
        securityService = new SecurityService() {
            @Override
            public int getSessionCacheMaxIdleSeconds() {
                return 1;
            }

            @Override
            public int getSessionTTLSeconds() {
                return 1;
            }
        };

        userRepo = Repository.getInstance(User.class);
        johnDoe = userRepo.findOneByQuery(new BasicDBObject("email", UserFixture.JOHN_DOE.getUser().getEmail())).get();

        token = new AuthToken(johnDoe.get_id(), sessionDurationInMonths);
        if (hazelcastInstance == null) {
            hazelcastInstance = Hazelcast.newHazelcastInstance(securityService.getSessionCacheConfig());
        }
        cache = hazelcastInstance.getMap(SecurityService.AUTH_TOKEN_MAP);
    }

    @AfterEach
    public void testTearDown() {
        securityService.logout(token);
    }

    @AfterAll
    public static void tearDownClass() {
        hazelcastInstance.shutdown();
    }

    @Test
    public void testAuthenticate() throws Exception {
        token = securityService.authenticate(johnDoe.getEmail(), "foobar");

        assertEquals(johnDoe.get_id(), token.getUser_id());
        assertTrue(token.getToken().length() > 50);

        securityService.authenticate(token.getToken());
    }

    @Test
    public void testAuthenticationIdempotency() {
        User john = UserFixture.JOHN_DOE.getUser();

        // authenticating twice should now throw any errors
        securityService.authenticate(john.getEmail(), "foobar");
        securityService.authenticate(john.getEmail(), "foobar");
    }

    @Test
    public void testAuthenticationFailure() throws Exception {
        Assertions.assertThrows(AuthDeniedException.class, () -> {
            User john = UserFixture.JOHN_DOE.getUser();
            securityService.authenticate(john.getEmail(), "oobar");
        });
    }

    @Test
    public void testGetUserInformation() {
        Assertions.assertThrows(AuthDeniedException.class, () -> securityService.authenticate("unexistingToken"));
    }

    @Test
    @Disabled("TODO: fix security service session cache logic")
    public void testSessionCache() {
        // on authenticate, SessionCache should be populated
        token = securityService.authenticate(johnDoe.getEmail(), "foobar");

        // cache should have John's info
        SessionInfo sessionInfo = cache.get(token.getToken());
        assertEquals(johnDoe.getEmail(), sessionInfo.getUserEmail());
        assertEquals(johnDoe.getName(), sessionInfo.getUserFullName());
        assertArrayEquals(new String[]{"USER"}, sessionInfo.getUserRoles());


        // on logout, SessionCache should be removed
        securityService.logout(token);
        // and token needs to be expired
        sessionInfo = cache.get(token.getToken());
        assertNull(sessionInfo);
    }

    @Test
    @Disabled("TODO: fix security service session cache logic")
    public void testSessionCacheEviction() throws Exception {
        testAuthenticate();

        SessionInfo sessionInfo = cache.get(token.getToken());
        Assertions.assertNotNull(sessionInfo);
        assertEquals(johnDoe.getName(), sessionInfo.getUserFullName());

        // let's sleep for two seconds and test cache eviction
        Thread.sleep(2000);

        assertNull(cache.get(token.getToken()));
    }

}