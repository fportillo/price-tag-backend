package br.com.almana.service;

import br.com.almana.domain.Item;
import br.com.almana.domain.ShoppingList;
import br.com.almana.domain.User;
import br.com.almana.repository.Repository;
import br.com.almana.util.fixture.ProductFixture;
import br.com.almana.util.fixture.UserFixture;
import com.mongodb.MongoWriteException;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


/**
 * Integration Test for User
 * Created by francisco on 11/1/15.
 */
public class UserServiceIT extends BaseIT {


    private final UserService service;
    private final Repository<User> repository;

    public UserServiceIT() {
        this.service = UserService.getInstance();
        this.repository = Repository.getInstance(User.class);
    }

    @Test
    public void testSaveShoppingList() throws Exception {
        User me = repository.findFirst();
        String _id = me.get_id();
        assertNotNull(_id);

        assertEquals(0, me.getShoppingLists().size());

        final Item papelJovita = ProductFixture.FILTRO_PAPEL_JOVITA_DALBEN.getProduct().asItem();
        final Item dietWay = ProductFixture.DIET_WAY.getProduct().asItem();
        Item[] barCodes = {papelJovita, dietWay};
        ShoppingList first = ShoppingList.newInstance("My First Shopping List", Optional.of(Arrays.asList(barCodes)));

        service.saveShoppingList(first, _id);

        User updatedMe = repository.findFirst();

        assertEquals(1, updatedMe.getShoppingLists().size());

        ShoppingList persistedList = updatedMe.getShoppingLists().iterator().next();

        assertTrue(persistedList.getItems().contains(papelJovita));
        assertTrue(persistedList.getItems().contains(dietWay));
        assertEquals(1, updatedMe.getShoppingLists().size());
        assertEquals(2, updatedMe.getShoppingLists().iterator().next().size());

        barCodes = new Item[]{
                ProductFixture.DETERGENTE_LIMPOL_CRISTAL_DALBEN.getProduct().asItem(),
                ProductFixture.AMACIENTE_IPE_INTENSO_DALBEN.getProduct().asItem(),
                ProductFixture.ANA_MARIA_CHOCOLATE.getProduct().asItem()
        };
        ShoppingList newShoppingList = ShoppingList.newInstance("My Second Shopping List", Optional.of(Arrays.asList(barCodes)));

        service.saveShoppingList(newShoppingList, _id);

        updatedMe = repository.findFirst();

        assertEquals(2, updatedMe.getShoppingLists().size());

        persistedList = updatedMe.getShoppingLists().iterator().next();

        assertTrue(persistedList.getItems().contains(papelJovita));
        assertTrue(persistedList.getItems().contains(dietWay));
        assertEquals(2, updatedMe.getShoppingLists().size());

        updatedMe.getShoppingLists().forEach(
                (sl) -> {
                    switch (sl.getName()) {
                        case "My First Shopping List":
                            assertEquals(2, sl.size());
                            break;
                        case "My Second Shopping List":
                            assertEquals(3, sl.size());
                            break;
                        default:
                            fail();
                    }
                }
        );

    }


    @Test
    public void testUpdateShoppingLists() {
        User me = repository.findFirst();

        final Item bicLotion = ProductFixture.LOCAO_POS_BARBA_BIC.getProduct().asItem();
        Item[] barcodeArray = {bicLotion, ProductFixture.LOCAO_POS_BARBA_BOZZANO.getProduct().asItem()};
        List<Item> barCodeList = Arrays.asList(barcodeArray);
        Optional<List<Item>> barcodes = Optional.of(barCodeList);
        ShoppingList original = ShoppingList.newInstance("To be updated test list", barcodes);

        me.add(original);

        final ShoppingList shoppingList = service.updateShoppingList(me.get_id(), original);

        assertEquals(original, shoppingList);

        // modify shopping list by removing one element
        original.getItems().remove(bicLotion);

        // one less element
        assertEquals(1, original.size());

        final ShoppingList updatedShoppingList = service.updateShoppingList(me.get_id(), original);

        assertEquals(original, updatedShoppingList);

        me = repository.get(new ObjectId(me.get_id())).get();

        assertEquals(updatedShoppingList, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get());

    }

    @Test
    public void testDeleteShoppingList() {
        User me = repository.findFirst();
        ObjectId myId = new ObjectId(me.get_id());

        ShoppingList newOne = ShoppingList.newInstance("Whatever list", Optional.empty());

        me.getShoppingLists().clear();

        me = repository.replace(me);

        me.add(newOne);

        service.updateShoppingList(me.get_id(), newOne);

        me = repository.get(myId).get();

        assertEquals(newOne, me.getShoppingLists().iterator().next());

        service.deleteShoppingList(me.get_id(), newOne.getUuid().toString());

        me = repository.get(myId).get();

        assertEquals(0, me.getShoppingLists().size());
    }

    @Test
    public void testSaveUser() {
        Assertions.assertThrows(MongoWriteException.class, () -> {
            User johnDoe = UserFixture.JOHN_DOE.getUser();
            johnDoe.set_id(null);
            // already saved on BeforeClass. should throw a mongo exception because of the repeated email.
            service.save(johnDoe);
        });
    }
}