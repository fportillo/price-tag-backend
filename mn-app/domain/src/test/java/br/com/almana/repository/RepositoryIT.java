package br.com.almana.repository;

import br.com.almana.domain.Location;
import br.com.almana.domain.Product;
import br.com.almana.domain.Shop;
import br.com.almana.service.BaseIT;
import br.com.almana.util.fixture.LocationFixture;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class RepositoryIT extends BaseIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryIT.class);
    private Product product;

    @Test
    public void testSave() {
        LOGGER.debug("Init testSave.");
        final double shopLatitude = -22.8800999;
        final double shopLongitude = -47.0437072;
        final double difference = 0.0000005;
        Shop shop = Shop.newInstance(shopLatitude, shopLongitude, "Dalben Supermercados - Unidade Taquaral");
        product = Product.newInstance("7898080640611", "Gilette Mach 3", new BigDecimal(32.50), shop.getLocation(),
            "https://aws.foo.dummy/bucket/address", false);
        product.setLocation(Location.getInstance(shopLatitude + difference, shopLongitude + difference));

        final Repository<Product> priceRepository = Repository.getInstance(Product.class);
        priceRepository.save(product);

        Optional<Product> retrievePrice = priceRepository.get(new ObjectId(product.get_id()));

        assertEquals(product.getBarCode(), retrievePrice.orElseThrow(AssertionError::new).getBarCode());
        LOGGER.debug("Finished testSave");
    }

    @Test
    public void testSaveWithExistingId() {
        Assertions.assertThrows(IllegalStateException.class, () -> {
            Location location = LocationFixture.ARARAQUARA.getLocation();
            product = Product.newInstance("7898080640611", "Gilette Mach 3", new BigDecimal(32.50), location,
                "https://aws.foo.dummy/bucket/address", false);
            product.setLocation(location);

            // if we set id, we expect an IllegalArgumentException to happen
            product.set_id("whateverObjectIdHexString");


            final Repository<Product> priceRepository = Repository.getInstance(Product.class);
            priceRepository.save(product);
        });
    }

    @Test
    public void testSaveInvalidPojos() {
        Assertions.assertThrows(ValidationException.class, () -> {
            Shop shop = Shop.newInstance(-200.00, 300.00, "Out of this world store");
            product = Product.newInstance("invalidBarcode", "", new BigDecimal(-1.00), shop.getLocation(), null, false);
            product.setShop(shop);
            final Repository<Product> repository = Repository.getInstance(Product.class);
            repository.save(product);
        });
    }

    @Test
    public void testReplaceOne() {
        Product product = Product.newInstance("7898080640611", "foo", new BigDecimal("1.99"), LocationFixture.ARARAQUARA.getLocation(), null, false);
        Repository<Product> repo = Repository.getInstance(Product.class);
        assertNull(product.get_id());
        product = repo.save(product);
        assertNotNull(product.get_id());
        assertEquals("foo", product.getName(), "Name mismatch");

        Optional<Product> maybeProduct = repo.get(new ObjectId(product.get_id()));
        assertEquals("foo", maybeProduct.get().getName());

        product.setName("bar");

        product = repo.replace(product);

        Optional<Product> maybeUpdated = repo.get(new ObjectId(product.get_id()));
        assertEquals("bar", maybeUpdated.get().getName());
    }

    @AfterAll
    public static void tearDownClass() {
        Mongo.INSTANCE.productCollection().drop();
    }
}