package br.com.almana.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by francisco on 3/15/16.
 */
public class SearchServiceIT extends BaseIT {

    private SearchService service;

    @BeforeEach
    public void setup() {
        service = new SearchService();
    }

    @Test
    public void testSearch() {
        // TODO add more assertions and make a good use case here!!!
        assertEquals(3, service.searchBestPrices(-22.8785233, -47.0464989, 30000, new String[]{"4005808806430", "7896098903032", "7891022100372", "7501006705928", "7891021002165", "7891080112836", "7893000439825"}).size());
    }


}