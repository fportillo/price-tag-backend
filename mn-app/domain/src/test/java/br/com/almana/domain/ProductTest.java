package br.com.almana.domain;

import br.com.almana.util.fixture.ProductFixture;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by francisco on 10/24/15.
 */
public class ProductTest {

    @Test
    public void testNormalizedName() {
        Optional<Location> location = Optional.empty();
        Product product = Product.newInstance("invalid", "Pão", BigDecimal.ZERO, location.orElse(null), null, false);
        assertEquals("Pao", product.getNormalizedName());
    }

    @Test
    public void testProductPricePrecision() {
        final BigDecimal value = new BigDecimal("1.99");
        Optional<Location> location = Optional.empty();
        Product product = Product.newInstance("invalid", "Loção", value, location.orElse(null), null, false);
        assertEquals(new BigDecimal("1.99").setScale(2, BigDecimal.ROUND_HALF_UP), product.getPrice());
    }

    @Test
    public void testGetUri() {
        Product product = ProductFixture.DIET_WAY.getProduct();
        assertEquals("/product/" + product.get_id(), product.getResourceUri());
    }

    @Test
    public void testNotVerifiedByDefault() {
        Product product = new Product();
        assertFalse(product.isVerified());
    }

    @Test
    @Disabled("Intermitent test probably because the way dates are being compared")
    public void testDates() {
        Product product = new Product();
        final Date dateCreated = product.getDateCreated();
        assertNotNull(dateCreated);
        product.setUpdated();
        assertTrue(product.getLastUpdate().compareTo(dateCreated) > 0);
    }
}