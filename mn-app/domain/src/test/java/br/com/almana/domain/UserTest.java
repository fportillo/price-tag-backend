package br.com.almana.domain;

import br.com.almana.util.fixture.ProductFixture;
import br.com.almana.util.fixture.UserFixture;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class UserTest {

    @Test
    public void testGetShoppingListUri() {
        ShoppingList shoppingList = makeShoppingList("Bla Ble Bloom");
        User me = UserFixture.JOHN_DOE.getUser();
        me.set_id("foo-id");
        assertEquals("/user/foo-id/shopping-list/" + shoppingList.getUuid(), me.getShoppingListUri(shoppingList));
    }

    @Test
    public void testRemoveShoppingList() {
        User me = UserFixture.JOHN_DOE.getUser();

        final ShoppingList comida = makeShoppingList("Comida");
        final ShoppingList bebida = makeShoppingList("Bebida");
        final ShoppingList roupa = makeShoppingList("Roupa");
        final ShoppingList doces = makeShoppingList("Doces");

        me.add(comida);
        me.add(bebida);
        me.add(roupa);
        me.add(doces);

        assertEquals(4, me.getShoppingLists().size());
        assertTrue(me.getShoppingLists().contains(roupa));

        me.removeShoppingList(roupa.getUuid());

        assertEquals(3, me.getShoppingLists().size());
        assertFalse(me.getShoppingLists().contains(roupa));

        assertTrue(me.getShoppingLists().contains(comida));
        assertTrue(me.getShoppingLists().contains(bebida));
        assertTrue(me.getShoppingLists().contains(doces));

    }

    @Test
    public void testUpdateShoppingList() {
        final ShoppingList original = makeShoppingList("Original");
        final ShoppingList whatever = makeShoppingList("Whatever");

        User me = UserFixture.JOHN_DOE.getUser();
        me.add(original);
        me.add(whatever);

        assertEquals(0, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().size());
        assertEquals(0, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(whatever.getUuid())).findAny().get().size());

        final Item dietWayBarCode = ProductFixture.DIET_WAY.getProduct().asItem();
        final Item anotherBarCode = ProductFixture.ANA_MARIA_CHOCOLATE.getProduct().asItem();
        original.addItem(dietWayBarCode);
        original.addItem(anotherBarCode);

        me.updateShoppingList(original);

        assertEquals(2, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().size());
        assertTrue(me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().getItems().contains(dietWayBarCode));
        assertTrue(me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().getItems().contains(anotherBarCode));
        assertEquals(0, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(whatever.getUuid())).findAny().get().size());

        original.removeItem(anotherBarCode);

        final ShoppingList modifiedOriginal = makeShoppingList("Modified Original");
        modifiedOriginal.setItems(original.getItems());
        modifiedOriginal.setUuid(original.getUuid());

        me.updateShoppingList(modifiedOriginal);
        assertEquals(1, me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().size());
        assertEquals("Modified Original", me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().getName());
        assertTrue(me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().getItems().contains(dietWayBarCode));
        assertFalse(me.getShoppingLists().stream().filter((sl) -> sl.getUuid().equals(original.getUuid())).findAny().get().getItems().contains(anotherBarCode));

    }

    private ShoppingList makeShoppingList(String name) {
        return ShoppingList.newInstance(name, Optional.empty());
    }

}