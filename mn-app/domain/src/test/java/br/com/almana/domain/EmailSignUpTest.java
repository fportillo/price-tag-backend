package br.com.almana.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;

/**
 * Created by francisco on 2/7/16.
 */
public class EmailSignUpTest {

    private EmailSignUp signUp;

    @BeforeEach
    public void setup() {
        signUp = new EmailSignUp();
    }

    @Test
    public void testEmptyEmailEmailSignUp() {
        Assertions.assertThrows(ValidationException.class, () -> {
            signUp.setPassword("12345678");
            signUp.validate();
        });
    }

    @Test
    public void testInvalidEmailEmailSignUp() {
        Assertions.assertThrows(ValidationException.class, () -> {
            signUp.setPassword("12345678");
            signUp.setPassword("foo");
            signUp.validate();
        });
    }

    @Test
    public void testShortPasswordEmailSignUp() {
        Assertions.assertThrows(ValidationException.class, () -> {
            signUp.setPassword("1234");
            signUp.setEmail("foo@bar");
            signUp.validate();
        });
    }

    @Test
    public void testBlankNameEmailSignUp() {
        Assertions.assertThrows(ValidationException.class, () -> {
            signUp.setEmail("foo@bar");
            signUp.setPassword("12345678");
            signUp.setName("");
            signUp.validate();
        });
    }

    @Test
    public void testValidEmailSignUp() {
        signUp.setEmail("foo@bar");
        signUp.setPassword("12345678");
        signUp.setName("chico");
        signUp.setConfirmationToken("fakect");
        signUp.setConfirmationLink("fakecl");
        signUp.validate();
    }

}