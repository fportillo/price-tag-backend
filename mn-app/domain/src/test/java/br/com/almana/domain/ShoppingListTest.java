package br.com.almana.domain;

import br.com.almana.util.fixture.ProductFixture;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Created by francisco on 10/27/15.
 */
public class ShoppingListTest {

    @Test
    public void testOperations() {
        ShoppingList list = ShoppingList.newInstance("Comida", Optional.empty());
        assertEquals(0, list.size());
        assertEquals("Comida", list.getName());

        final Item amaciante = ProductFixture.AMACIENTE_IPE_INTENSO_DALBEN.getProduct().asItem();

        // adding 10 times same barcode, does not modify list
        loopAssert((i) -> {
                    list.addItem(amaciante);
                    assertEquals(1, list.size());
                    assertEquals(amaciante, list.getItems().stream().findFirst().get());
                }
        );

        final Item anotherItem = ProductFixture.LASANHA_BOLONHESA_SADIA_DALBEN.getProduct().asItem();
        list.addItem(anotherItem);
        assertEquals(2, list.size());

        Item[] someBcodes = {
                ProductFixture.DETERGENTE_LIMPOL_CRISTAL_DALBEN.getProduct().asItem(),
                ProductFixture.DIET_WAY.getProduct().asItem(),
                ProductFixture.FILTRO_PAPEL_JOVITA_DALBEN.getProduct().asItem()
        };

        final List<Item> pBarCodes = Arrays.asList(someBcodes);
        list.addItems(pBarCodes);
        assertEquals(5, list.size());

        // removing same bCode 10 times should only remove once
        loopAssert((i) -> {
            list.removeItem(anotherItem);
            assertEquals(4, list.size());
        });

        // same for removeItems...
        loopAssert((i) -> {
            list.removeItems(pBarCodes);
            assertEquals(1, list.size());
        });

        list.clear();
        assertEquals(0, list.size());

    }

    private void loopAssert(IntConsumer action) {
        IntStream.range(0, 10).forEach(action::accept);
    }

}