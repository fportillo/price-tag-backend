package br.com.almana.domain;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AuthTokenTest {

    private AuthToken token;

    @BeforeEach
    public void setup() {
        token = new AuthToken("foo", 12);
    }

    @Test
    public void testExpiresAtConfigured() {
        // this test may fail if January 1st comes in the middle of it :)
        assertEquals("foo", token.getUser_id());
        Calendar cal = Calendar.getInstance();
        cal.setTime(token.getCreatedAt());
        cal.add(Calendar.YEAR, 1);
        assertEquals(format(cal.getTime()), new SimpleDateFormat("dd/MM/yyyy").format(token.getExpiresAt()));
    }

    private static String format(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }
}