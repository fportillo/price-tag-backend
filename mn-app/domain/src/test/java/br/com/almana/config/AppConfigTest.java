package br.com.almana.config;


import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Test for configuration class
 * Created by francisco on 9/28/15.
 */
public class AppConfigTest {

    @Test
    public void testLoadFromFileInFileSystem() throws IOException {

        final String filePath = "/tmp/_testConfigLoadFromFs.properties";
        final String fileContent = "mongoHost=aws2-ec2-host\n" +
                "mongoDatabase=pricetag\n" +
                "mongoUri=mongodb://localhost:27017/pricetag\n" +
                "maxRequestsPerSecond=10\n" +
                "throttleCacheExpireTimeInSecs=300\n" +
                "pageSize=10\n" +
                "rabbitHost=localhost\n" +
                "mailFrom=noreply\n" +
                "domain=dummydomain\n" +
                "defaultDistanceRange=100000\n" +
                "# auth token configuration\n" +
                "authTokenExpiresAfterYears = 1\n";
        Files.write(Paths.get(filePath), fileContent.getBytes());

        System.setProperty("config.properties", filePath);

        assertEquals("aws2-ec2-host", AppConfig.newInstance().getMongoHost());
    }

}