package br.com.almana.util;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by francisco on 10/22/15.
 */
public class StringNormalizerTest {

    public static String accentedChars = "À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï ð ñ ò ó ô õ ö ø ù ú û ü ý þ ÿ";

    @Test
    public void testAccentRemoval() {
        assertEquals("A A A A A A Æ C E E E E I I I I Ð N O O O O O Ø U U U U Y Þ ß a a a a a a æ c e e e e i i i i ð n o o o o o ø u u u u y þ y", StringNormalizer.removeAccents(accentedChars));
        assertEquals("c", StringNormalizer.removeAccents("ç"));
        assertEquals("e", StringNormalizer.removeAccents("é"));
        assertEquals("a", StringNormalizer.removeAccents("à"));
        assertEquals("cao", StringNormalizer.removeAccents("ção"));
        assertEquals("CaO", StringNormalizer.removeAccents("ÇãO"));
        assertEquals("u", StringNormalizer.removeAccents("ü"));
        assertEquals("pao", StringNormalizer.removeAccents("pão"));
        assertEquals("cafe", StringNormalizer.removeAccents("café"));
        assertEquals("biscoito", StringNormalizer.removeAccents("biscoito"));
    }

}