package br.com.almana.service;

import br.com.almana.domain.Location;
import br.com.almana.domain.Product;
import br.com.almana.util.StringNormalizer;
import br.com.almana.util.fixture.LocationFixture;
import br.com.almana.util.fixture.ProductFixture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by francisco on 10/13/15.
 */
public class ProductServiceIT extends BaseIT {

    private ProductService service;

    @BeforeEach
    public void setup() {
        service = new ProductService();
    }

    @Test
    public void findTopByBarcode() {
        List<Product> topProducts = service.findTopByBarcode("none");

        assertEquals(0, topProducts.size(), "No prices should have been returned");


        topProducts = service.findTopByBarcode("7898080640611");
        assertEquals(4, topProducts.size(), "Wrong returned result size");

        // ordering
        // first by verified (true first, false second)
        // second by price desc
        assertEquals(ProductFixture.LEITE_ITALAC_SAMS_CLUB.getProduct().getPrice(), topProducts.get(0).getPrice());
        assertEquals(ProductFixture.LEITE_ITALAC_PAO_DE_ACUCAR.getProduct().getPrice(), topProducts.get(1).getPrice());
        assertEquals(ProductFixture.LEITE_ITALAC_PAULISTAO_FROM_USER.getProduct().getPrice(), topProducts.get(2).getPrice());
        assertEquals(ProductFixture.LEITE_ITALAC_DALBEN_FROM_USER.getProduct().getPrice(), topProducts.get(3).getPrice());
    }

    @Test
    public void testFindTopByName() {
        assertSuggestionSearch("cHoC");
        assertSuggestionSearch("ChOCOlaTE");
        assertSuggestionSearch("ÇãO");
        assertSuggestionSearch("LOçÃo");
        assertSuggestionSearch("pao");
    }

    @Test
    public void testFindNearByBarcode() {
        String barCode = ProductFixture.LEITE_ITALAC_DALBEN_FROM_USER.getProduct().getBarCode();
        Location ruaAraraquara191 = LocationFixture.ARARAQUARA.getLocation();

        List<Product> nearProducts = service.findNearByBarcode(barCode, ruaAraraquara191, 0);
        assertEquals(0, nearProducts.size());

        nearProducts = service.findNearByBarcode(barCode, ruaAraraquara191, 100);
        assertEquals(0, nearProducts.size());

        nearProducts = service.findNearByBarcode(barCode, ruaAraraquara191, 1000);
        assertEquals(1, nearProducts.size());

        nearProducts = service.findNearByBarcode(barCode, ruaAraraquara191, 10000);
        assertTrue(nearProducts.size() > 1);

        // assert barCode and sorting by value asc
        nearProducts.forEach((p) -> {
            BigDecimal value = new BigDecimal(0.00);
            assertEquals(barCode, p.getBarCode());
            assertTrue(value.compareTo(p.getPrice()) < 0);
            value = p.getPrice();
        });
    }

    @Test
    public void testFindNearByName() {
        String name = ProductFixture.LEITE_ITALAC_DALBEN_FROM_USER.getProduct().getName();
        Location ruaAraraquara191 = Location.getInstance(-22.8815712, -47.0471097);

        List<Product> nearProducts = service.findNearByName(name, ruaAraraquara191, 0);
        assertEquals(0, nearProducts.size());

        nearProducts = service.findNearByName(name, ruaAraraquara191, 100);
        assertEquals(0, nearProducts.size());

        nearProducts = service.findNearByName(name, ruaAraraquara191, 1000);
        assertEquals(1, nearProducts.size());

        nearProducts = service.findNearByName(name, ruaAraraquara191, 10000);
        assertTrue(nearProducts.size() > 1);

        // assert barCode and sorting by value asc
        final BigDecimal[] value = {new BigDecimal(0.00)};
        nearProducts.forEach((p) -> {
            assertEquals(name, p.getName());
            assertTrue(value[0].compareTo(p.getPrice()) < 0);
            value[0] = p.getPrice();
        });

    }

    @Test
    public void testQueryLimit() {

    }

    private void assertSuggestionSearch(String query) {
        List<Product> topByName = service.findTopByNameLike(query);
        assertTrue(topByName.size() > 0);
        topByName.forEach((p) -> assertTrue(p.getName().toLowerCase().contains(query.toLowerCase()) ||
                p.getNormalizedName().toLowerCase().contains(StringNormalizer.removeAccents(query).toLowerCase())));
    }

    @Test
    public void testSaveInValid() {
        Assertions.assertThrows(ValidationException.class, () -> {
            Location location = null;
            service.save(Product.newInstance(null, "", new BigDecimal(0.00), location, null, false));
        });
    }

    @Test
    public void testSaveValid() {
        Location location = LocationFixture.ARARAQUARA.getLocation();
        Product product = Product.newInstance(ProductFixture.DIET_WAY.getProduct().getBarCode(), "Foo name", new BigDecimal(9.99), location, null, false);
        product.set_id(null);
        final URI uri = service.save(product);
        assertTrue(uri.getPath().startsWith("/product"));
    }
}