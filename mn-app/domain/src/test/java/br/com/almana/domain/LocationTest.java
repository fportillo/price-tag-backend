package br.com.almana.domain;

import br.com.almana.util.fixture.ShopFixture;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Created by francisco on 10/24/15.
 */
public class LocationTest {


    @Test
    public void testDistanceTo() throws Exception {
        final Shop dalben = ShopFixture.DALBEN_TAQUARAL.getShop();
        final Shop sams = ShopFixture.SAMS_CLUB_CAMPINAS.getShop();
        final Shop paulistao = ShopFixture.PAULISTAO_OROZIMBO_MAIA.getShop();
        final Shop drogasilTaquaral = ShopFixture.DROGASIL_TAQUARAL.getShop();

        // distance to the same place should be zero
        assertEquals(0.00, dalben.getLocation().distanceTo(dalben.getLocation()), 0.00000005);

        assertEquals(3100, dalben.getLocation().distanceTo(sams.getLocation()), 300);

        assertEquals(5400, paulistao.getLocation().distanceTo(sams.getLocation()), 500);

        assertEquals(83, dalben.getLocation().distanceTo(drogasilTaquaral.getLocation()), 50);
    }
}