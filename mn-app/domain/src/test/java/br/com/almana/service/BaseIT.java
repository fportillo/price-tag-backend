package br.com.almana.service;

import br.com.almana.util.DataLoader;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for *Service class integration tests.
 * Created by francisco on 11/1/15.
 */
public class BaseIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseIT.class);

    @BeforeAll
    public static void bootstrap() {
        DataLoader.cleanup();
        LOGGER.debug("Bootstrapping for integration tests...");
        DataLoader.load();
        LOGGER.debug("Bootstrap finished");
    }

    @AfterAll
    public static void tearDown() {
        DataLoader.cleanup();
    }

}
