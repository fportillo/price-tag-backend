package br.com.almana.domain;

/**
 * Generic interface to make implementing instances do some work that needs to be done before they are persisted.
 * Created by francisco on 11/8/15.
 */
public interface Updatable {

    /**
     * Callback provided to implementations that are going to be saved.
     */
    void onSave();
}
