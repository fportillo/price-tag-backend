package br.com.almana.config;

import br.com.almana.util.ValidatorWrapper;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Min;
import java.util.Properties;

/**
 * Configuration object
 * Created by francisco on 9/28/15.
 */
public class AppConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);
    private static final String PROP_PARAM = "config.properties";

    private static AppConfig instance;

    private final Properties properties;

    @Range(min = 1, max = 100) // same user max 100 requests/sec
    private Integer maxRequestsPerSecond;

    @Range(min = 1, max = 86400)
    private final int throttleCacheExpireTimeInSecs;
    @NotBlank
    private final String mongoHost;

    @NotBlank
    private final String mongoDatabase;

    @NotBlank
    private final String mongoUri;

    @NotBlank
    private final String rabbitHost;

    @NotBlank
    private final String domain;

    @NotBlank
    private final String mailFrom;

    @Range(min = 1, max = 100)
    private final int pageSize;

    @Range(min = 0, max = 100000)
    private final int defaultDistanceRange;

    @Min(0)
    private final long authTokenExpiresAfterYears;


    private AppConfig() {
        // First lookup for a JVM prop
        properties = loadProperties();

        mongoHost = properties.getProperty("mongoHost");
        mongoDatabase = properties.getProperty("mongoDatabase");
        mongoUri = properties.getProperty("mongoUri");
        maxRequestsPerSecond = Integer.parseInt(properties.getProperty("maxRequestsPerSecond"));
        throttleCacheExpireTimeInSecs = Integer.parseInt(properties.getProperty("throttleCacheExpireTimeInSecs"));
        pageSize = Integer.parseInt(properties.getProperty("pageSize"));
        defaultDistanceRange = Integer.parseInt(properties.getProperty("defaultDistanceRange"));
        authTokenExpiresAfterYears = Long.parseLong(properties.getProperty("authTokenExpiresAfterYears"));

        rabbitHost = properties.getProperty("rabbitHost");

        domain = properties.getProperty("domain");

        mailFrom = properties.getProperty("mailFrom");

        ValidatorWrapper.INSTANCE.validate(this);
    }

    private Properties loadProperties() {
        return AppPropertiesLoader.load(PROP_PARAM);
    }

    /**
     * Returns a new instance or cached one if already exists.
     *
     * @return
     */
    public static AppConfig getInstance() {
        if (instance == null) {
            instance = new AppConfig();
        }
        return instance;
    }

    /**
     * Always load and return a new config instance
     *
     * @return
     */
    public static AppConfig newInstance() {
        return new AppConfig();
    }

    public String getMongoHost() {
        return mongoHost;
    }

    public String getMongoDatabase() {
        return mongoDatabase;
    }

    public String getMongoUri() {
        return mongoUri;
    }

    public int getMaxRequestsPerSecond() {
        return maxRequestsPerSecond;
    }

    public int getThrottleCacheExpireTimeInSecs() {
        return throttleCacheExpireTimeInSecs;
    }

    public String getRabbitHost() {
        return rabbitHost;
    }

    public String getDomain() {
        return domain;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (Object key : properties.keySet()) {
            sb.append(String.format("%s = %s\n", key, properties.getProperty(key.toString())));
        }
        return sb.toString();
    }

    public int getPageSize() {
        return pageSize;
    }

    public Integer getDefaultDistanceRange() {
        return defaultDistanceRange;
    }

    public void setMaxRequestsPerSecond(Integer pMaxRequestsPerSecond) {
        maxRequestsPerSecond = pMaxRequestsPerSecond;
    }

    public Long getAuthTokenExpiresAfterYears() {
        return authTokenExpiresAfterYears;
    }

    public String getMailFrom() {
        return mailFrom;
    }
}
