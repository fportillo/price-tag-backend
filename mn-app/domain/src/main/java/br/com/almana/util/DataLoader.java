package br.com.almana.util;

import br.com.almana.domain.BaseEntity;
import br.com.almana.domain.Product;
import br.com.almana.domain.User;
import br.com.almana.repository.Mongo;
import br.com.almana.repository.Repository;
import br.com.almana.util.fixture.ProductFixture;
import br.com.almana.util.fixture.UserFixture;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.IndexOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Utility class to load development test data.
 * Created by laercio on 07/11/15.
 */
public class DataLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);

    public static void cleanup() {
        LOGGER.debug("Dropping mongo collections");
        Mongo.INSTANCE.productCollection().drop();
        Mongo.INSTANCE.userCollection().drop();
        Mongo.INSTANCE.authTokenCollection().drop();
        Mongo.INSTANCE.emailSignUpCollection().drop();
    }

    public static void load() {
        LOGGER.debug("Loading data for integration tests...");
        bootstrapProducts();
        bootstrapUsers();
        LOGGER.debug("Data load finished");
    }

    private static void bootstrapUsers() {
        Repository<User> repo = Repository.getInstance(User.class);
        for (UserFixture fix : UserFixture.values()) {
            safeSave(fix.getUser(), repo);
        }
    }

    private static void bootstrapProducts() {
        Repository<Product> repository = Repository.getInstance(Product.class);
        for (ProductFixture fixture : ProductFixture.values()) {
            safeSave(fixture.getProduct(), repository);
        }
        createIndexes();
    }

    /**
     * This method contains the indexes that need to be created in the mongo database.
     */
    // TODO: need to move this to the application deployment script when needed
    private static void createIndexes() {
        Mongo.INSTANCE.productCollection().createIndex(new BasicDBObject("location", "2dsphere"));

        IndexOptions uniqueOption = new IndexOptions();
        uniqueOption.unique(true);
        Mongo.INSTANCE.userCollection().createIndex(new BasicDBObject("email", 1), uniqueOption);

        Mongo.INSTANCE.authTokenCollection().createIndex(new BasicDBObject("user_id", 1), uniqueOption);

        // TODO: create an automated test for this.
        IndexOptions expiresAtOption = new IndexOptions();
        expiresAtOption.expireAfter(0L, TimeUnit.SECONDS);
        Mongo.INSTANCE.authTokenCollection().createIndex(new BasicDBObject("expiresAt", 1), expiresAtOption);
    }

    private static void safeSave(BaseEntity baseEntity, Repository repository) {
        baseEntity.set_id(null);
        repository.save(baseEntity);
    }

    public static void main(String[] args) {
        cleanup();
        load();
    }


}
