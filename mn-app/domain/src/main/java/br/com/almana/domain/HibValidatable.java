package br.com.almana.domain;

/**
 * Interface for those who wants to be validated using hibernate validation.
 * Created by francisco on 2/7/16.
 */
public interface HibValidatable {

    void validate();

}
