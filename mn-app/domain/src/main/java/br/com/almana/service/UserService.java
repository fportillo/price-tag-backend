package br.com.almana.service;

import br.com.almana.domain.ShoppingList;
import br.com.almana.domain.User;
import br.com.almana.exception.ShoppingListNotFoundException;
import br.com.almana.exception.UserNotFoundException;
import br.com.almana.repository.Repository;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * User related methods
 * Created by francisco on 10/28/15.
 */
@Singleton
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final Repository<User> repository;

    public UserService() {
        this.repository = Repository.getInstance(User.class);
    }


    public URI saveShoppingList(final ShoppingList shoppingList, final String userId) {
        User shoppingListOwner = getUser(userId);
        shoppingList.validate();
        shoppingListOwner.add(shoppingList);

        updateUserShoppingLists(shoppingListOwner);
        return URI.create(shoppingListOwner.getShoppingListUri(shoppingList));
    }

    private void updateUserShoppingLists(User shoppingListOwner) {
        List<Document> mongoListAdapter = new ArrayList<>();
        mongoListAdapter.addAll(shoppingListOwner.getShoppingLists()
                .stream()
                .map((sl) -> Document.parse(new Gson().toJson(sl))).collect(Collectors.toList()));

        // save the entire document here, which is the full user
        repository.update(shoppingListOwner, new BasicDBObject("shoppingLists", mongoListAdapter));
    }

    private User getUser(String userId) {
        Optional<User> owner = repository.get(new ObjectId(userId));
        return owner.orElseThrow(new UserNotFoundException(userId));
    }

    public ShoppingList deleteShoppingList(final String userId, final String shoppingListId) {
        User user = getUser(userId);
        ShoppingList deleted = user.removeShoppingList(UUID.fromString(shoppingListId));
        updateUserShoppingLists(user);
        return deleted;
    }

    public ShoppingList updateShoppingList(final String userId, final ShoppingList shoppingList) {
        User user = getUser(userId);
        shoppingList.validate();
        user.updateShoppingList(shoppingList);
        updateUserShoppingLists(user);
        return shoppingList;
    }

    public ShoppingList getShoppingList(String userId, final String shoppingListId) {
        return getUser(userId)
                .getShoppingLists()
                .stream()
                .filter((sl) -> sl.getUuid().toString().equals(shoppingListId))
                .findFirst()
                .orElseThrow(new ShoppingListNotFoundException(shoppingListId));
    }

    public List<ShoppingList> getShoppingLists(final String userId) {
        return getUser(userId)
                .getShoppingLists()
                .stream()
                .sorted((sl, other) -> sl.getName().compareTo(other.getName()))
                .collect(Collectors.toList());
    }

    public static UserService getInstance() {
        return new UserService();
    }

    public void save(User newOne) {
        repository.save(newOne);
    }

}
