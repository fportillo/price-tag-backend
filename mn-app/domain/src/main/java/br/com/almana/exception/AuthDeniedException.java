package br.com.almana.exception;

import java.util.function.Supplier;

/**
 * Exception to throw when authentication fails.
 * Created by francisco on 12/2/15.
 */
public class AuthDeniedException extends RuntimeException implements Supplier<RuntimeException> {
    @Override
    public RuntimeException get() {
        return this;
    }
}
