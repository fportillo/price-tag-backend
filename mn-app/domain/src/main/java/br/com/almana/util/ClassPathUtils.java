package br.com.almana.util;

import java.io.InputStream;
import java.util.Scanner;

/**
 *
 * Created by francisco on 3/8/16.
 */
public class ClassPathUtils {

    public static String readFileFromClasspath(String fullClasspath) {
        final InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream(fullClasspath);
        Scanner scanner = new Scanner(resourceAsStream);
        StringBuilder sb = new StringBuilder();
        while(scanner.hasNextLine()) {
            sb.append(scanner.nextLine());
        }
        scanner.close();
        return sb.toString();
    }

}
