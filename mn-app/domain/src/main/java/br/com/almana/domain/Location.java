package br.com.almana.domain;

import com.google.gson.Gson;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by francisco on 10/24/15.
 */
public class Location {

    private static final int EARTH_RADIUS = 6371; // Radius of the earth in meters

    private static final String MONGO_DB_COORDINATE_TYPE = "Point";

    @NotEmpty
    private double[] coordinates;

    @NotBlank
    private String type;

    @XmlTransient
    @Range(min = -90, max = 90)
    private transient double latitude;

    @XmlTransient
    @Range(min = -180, max = 180)
    private transient double longitude;

    private Location() {
    }

    public static Location getInstance(double latitude, double longitude) {
        Location instance = new Location();
        final double[] coords = new double[2];

        // on MongoDB longitude comes first, and latitude second

        coords[0] = longitude;
        coords[1] = latitude;
        instance.setCoordinates(coords);
        instance.setLongitude(coords[0]);
        instance.setLatitude(coords[1]);
        instance.setType(MONGO_DB_COORDINATE_TYPE);

        return instance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double distanceTo(final Location other) {
        double latDistance = deg2rad(other.getLatitude() - getLatitude());
        double lonDistance = deg2rad(other.getLongitude() - getLongitude());
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(deg2rad(getLatitude())) * Math.cos(deg2rad(other.getLatitude()))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = EARTH_RADIUS * c * 1000; // convert to meters

        distance = Math.pow(distance, 2);
        return Math.sqrt(distance);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
