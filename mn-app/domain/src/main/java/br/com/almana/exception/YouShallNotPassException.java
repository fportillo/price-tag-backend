package br.com.almana.exception;

/**
 * Exception raised when the user does not have the right roles to call a method.
 * Created by francisco on 12/3/15.
 */
public class YouShallNotPassException extends RuntimeException {
    public YouShallNotPassException(String message) {
        super(message);
    }
}
