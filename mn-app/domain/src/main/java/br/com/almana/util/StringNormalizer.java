package br.com.almana.util;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 * Created by francisco on 10/22/15.
 */
public class StringNormalizer {

    private static Pattern pattern = Pattern.compile("\\p{InCOMBINING_DIACRITICAL_MARKS}+");

    public static final String removeAccents(final String original) {
        String nfdNormalizedString = Normalizer.normalize(original, Normalizer.Form.NFD);
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

}
