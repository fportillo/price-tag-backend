package br.com.almana.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by francisco on 2/8/16.
 */
public class AppPropertiesLoader {

    private static final Logger LOG = LoggerFactory.getLogger(AppPropertiesLoader.class);


    public static Properties load(String propParam) {
        Properties properties = new Properties();
        String file = System.getProperty(propParam);
        try {
            if (valid(file)) {
                properties.load(new FileReader(file));
                LOG.info("Properties loaded from {}", file);
            } else {
                properties.load(AppPropertiesLoader.class.getClassLoader().getResourceAsStream(propParam));
                if (file != null) {
                    LOG.info("Properties loaded from classpath:{}", file);
                }
            }
        } catch (IOException e1) {
            LOG.error("Could not load properties", e1);
        }
        LOG.info("Properties loaded: {}", properties);
        return properties;

    }

    private static boolean valid(String file) {
        return StringUtils.isNotBlank(file) && Files.isRegularFile(Paths.get(file));
    }

}
