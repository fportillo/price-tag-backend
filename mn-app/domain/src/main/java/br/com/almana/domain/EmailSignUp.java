package br.com.almana.domain;

import br.com.almana.util.HashUtil;
import br.com.almana.util.ValidatorWrapper;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * POJO domain class to validate email sign up requests.
 * Created by francisco on 2/7/16.
 */
public class EmailSignUp extends BaseEntity {

    public static final String MONGO_COLLECTION_NAME = "email-signup";

    @Email
    @NotBlank
    private String email;

    @Length(min = 4, max = 64)
    private String password;

    @NotBlank
    private String name;

    @NotBlank
    private String confirmationToken;

    @NotBlank
    private String confirmationLink;

    @NotNull
    private Status status;

    private String emailFrom;

    public enum Status {
        NEW,
        BURNED
    }

    public EmailSignUp(String email, String password) {
        this.email = email;
        this.password = password;
        this.status = Status.NEW;
    }

    EmailSignUp() {
        this.status = Status.NEW;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void validate() {
        ValidatorWrapper.INSTANCE.validate(this);
    }

    @Override
    public void onSave() {
        // do nothing
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmationLink() {
        return confirmationLink;
    }

    public void setConfirmationLink(String confirmationLink) {
        this.confirmationLink = confirmationLink;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void burn() {
        setStatus(Status.BURNED);
    }
}
