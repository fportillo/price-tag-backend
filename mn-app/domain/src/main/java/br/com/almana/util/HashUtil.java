package br.com.almana.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

/**
 * Hash utility class.
 * Created by francisco on 11/30/15.
 */
public class HashUtil {

    private static final Logger LOG = LoggerFactory.getLogger(HashUtil.class);

    public static final int SALT_SEED_LENGTH = 32;
    public static final String ALGORITHM = "SHA-256";

    public static String hash(String password, String salt) {
        return hash(String.format("%s%s", salt, password));
    }

    public static String hash(String password) {
        try {
            MessageDigest digest;
            digest = MessageDigest.getInstance(ALGORITHM);
            byte[] hash = digest.digest(password.getBytes());
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Problems with HashUtil class.", e);
            throw new RuntimeException(e);
        }
    }

    public static String newSalt() {
        return Base64.getEncoder().encodeToString(new SecureRandom().generateSeed(SALT_SEED_LENGTH));
    }

    public static String newAuthToken() {
        Random random = new SecureRandom();
        return new BigInteger(260, random).toString(32);
    }
}
