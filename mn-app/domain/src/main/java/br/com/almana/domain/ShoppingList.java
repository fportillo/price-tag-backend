package br.com.almana.domain;

import br.com.almana.util.ValidatorWrapper;
import com.google.gson.Gson;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * This class represents a shopping list that is going to be saved for future search,
 * e.g., in case user removes app, then install and sign in again, shopping lists are kept.
 * Created by francisco on 10/27/15.
 */
public class ShoppingList implements Jsonifiable {

    public static final String PATH = "/shopping-list";

    @NotBlank
    @Size(min = 3, max = 30)
    private String name;

    @NotNull
    private UUID uuid;

    @Size(min = 0, max = 100) // defaulting max shopping list size to 100 items
    @Valid
    private Set<Item> items;

    private ShoppingList() {
        items = new LinkedHashSet<>();
        uuid = UUID.randomUUID();
    }

    public static ShoppingList newInstance(String name, Optional<List<Item>> barCodes) {
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.name = name;
        barCodes.ifPresent(shoppingList.items::addAll);
        return shoppingList;
    }

    public void addItem(final Item barCode) {
        items.add(barCode);
    }

    public void addItems(final List<Item> pBarCodes) {
        items.addAll(pBarCodes);
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public void removeItem(final Item bCode) {
        items.remove(bCode);
    }

    public void removeItems(final List<Item> pBarCodes) {
        items.removeAll(pBarCodes);
    }

    public void clear() {
        items.clear();
    }

    public int size() {
        return items.size();
    }

    public Set<Item> getItems() {
        return items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingList that = (ShoppingList) o;

        if (!name.equals(that.name)) return false;
        if (!uuid.equals(that.uuid)) return false;
        return items.equals(that.items);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void validate() {
        ValidatorWrapper.INSTANCE.validate(this);
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }

    @Override
    public String toString() {
        return toJson();
    }
}
