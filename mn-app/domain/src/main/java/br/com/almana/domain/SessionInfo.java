package br.com.almana.domain;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;

/**
 * Pojo for session information
 * Created by francisco on 12/5/15.
 */
public class SessionInfo implements DataSerializable {

    private static final Gson GSON = new Gson();

    // put more information here as needed

    private String userEmail;
    private String userFullName;
    private String[] userRoles;
    private AuthToken authToken;

    // FIXME see if there's a better way of serializing the user Role array
    private int userRolesSize;

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeUTF(userEmail);
        out.writeUTF(userFullName);
        out.writeInt(userRolesSize);
        for (int i = 0; i < userRolesSize; i++) {
            out.writeUTF(userRoles[i]);
        }
        out.writeUTF(GSON.toJson(authToken));
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        userEmail = in.readUTF();
        userFullName = in.readUTF();
        userRolesSize = in.readInt();
        userRoles = new String[userRolesSize];
        for (int i = 0; i < userRolesSize; i++) {
            userRoles[i] = in.readUTF();
        }
        authToken = GSON.fromJson(in.readUTF(), AuthToken.class);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String[] getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(String[] userRoles) {
        this.userRoles = userRoles;
        this.userRolesSize = userRoles.length;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }
}
