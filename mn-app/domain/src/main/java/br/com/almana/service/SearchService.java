package br.com.almana.service;

import br.com.almana.repository.Mongo;
import br.com.almana.util.JSFunction;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by francisco on 3/15/16.
 */
@Singleton
public class SearchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchService.class);

    public List<Document> searchBestPrices(double latitude, double longitude, int maxDistance, String[] barCodes) {
        BasicDBObject query = new BasicDBObject();
        BasicDBObject location = new BasicDBObject();
        BasicDBObject near = new BasicDBObject();
        BasicDBObject geometry = new BasicDBObject();

        geometry.put("type", "Point");
        // on MongoDB longitude is the first coordinate and latitude, the second.
        geometry.put("coordinates", new double[]{longitude, latitude});

        near.put("$geometry", geometry);
        near.put("$maxDistance", maxDistance);

        location.put("$near", near);

        query.put("location", location);

        BasicDBObject in = new BasicDBObject("$in", barCodes);

        query.put("barCode", in);

        final MongoCollection<Document> productCollection = Mongo.INSTANCE.productCollection();

        final MapReduceIterable<Document> documents = productCollection.mapReduce(JSFunction.SHOPPING_LIST_MAPPING.getValue(), JSFunction.SHOPPING_LIST_REDUCE.getValue())
                .filter(query);

        List<Document> documentList = new ArrayList<>();

        for (Document doc : documents) {
            documentList.add(doc);
        }
        return documentList;

    }
}
