package br.com.almana.repository;

import br.com.almana.config.AppConfig;
import br.com.almana.domain.*;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by francisco on 10/15/15.
 */
public enum Mongo {

    INSTANCE;

    private static final Logger LOGGER = LoggerFactory.getLogger(Mongo.class);

    private MongoClient mongoClient;

    Mongo() {
        this.mongoClient = new MongoClient(
            new MongoClientURI(AppConfig.getInstance().getMongoUri())
        );
    }

    public MongoCollection<Document> productCollection() {
        return this.mongoClient.getDatabase(AppConfig.getInstance().getMongoDatabase()).getCollection(Product.MONGO_COLLECTION_NAME);
    }

    public void close() {
        this.mongoClient.close();
    }

    public <T extends BaseEntity> MongoCollection<Document> collectionByType(Class<T> type) {
        if (type.equals(Product.class)) {
            return productCollection();
        }
        if (type.equals(User.class)) {
            return userCollection();
        }
        if (type.equals(AuthToken.class)) {
            return authTokenCollection();
        }
        if (type.equals(EmailSignUp.class)) {
            return emailSignUpCollection();
        }
        String errorMessage = String.format("Unsupported collection for class %s", type.getSimpleName());
        LOGGER.error(errorMessage);
        throw new IllegalArgumentException(errorMessage);
    }

    public MongoCollection<Document> emailSignUpCollection() {
        return this.mongoClient.getDatabase(AppConfig.getInstance().getMongoDatabase()).getCollection(EmailSignUp.MONGO_COLLECTION_NAME);
    }

    public MongoCollection<Document> authTokenCollection() {
        return this.mongoClient.getDatabase(AppConfig.getInstance().getMongoDatabase()).getCollection(AuthToken.MONGO_COLLECTION_NAME);
    }

    public MongoCollection<Document> userCollection() {
        return this.mongoClient.getDatabase(AppConfig.getInstance().getMongoDatabase()).getCollection(User.MONGO_COLLECTION_NAME);
    }
}
