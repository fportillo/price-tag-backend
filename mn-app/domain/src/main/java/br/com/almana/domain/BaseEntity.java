package br.com.almana.domain;

import br.com.almana.util.ValidatorWrapper;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Base entity for the classes that represent documents that are going to be persisted in MongoDB.
 * Created by francisco on 10/18/15.
 */
public abstract class BaseEntity implements Jsonifiable, Updatable {

    /**
     * Hex String representation Of Mongo's ObjectId pk.
     */
    private String _id;

    public BaseEntity() {
    }

    public void validate() {
        ValidatorWrapper.INSTANCE.validate(this);
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }

    public String get_id() {
        return this._id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
