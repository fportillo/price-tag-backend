package br.com.almana.domain;

import java.util.Arrays;
import java.util.List;

/**
 * User roles.
 * Created by francisco on 12/2/15.
 */
public enum Role {

    USER,
    ROOT,


    // test role
    NOBODY_SHOULD_HAVE_THIS_ROLE;

    public static List<Role> defaultRoles() {
        return Arrays.asList(new Role[]{USER});
    }
}
