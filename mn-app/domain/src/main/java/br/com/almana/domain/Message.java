package br.com.almana.domain;

import br.com.almana.util.ValidatorWrapper;
import com.google.gson.Gson;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by francisco on 2/7/16.
 */
public class Message implements Jsonifiable, HibValidatable {

    @NotNull
    private Queue destination;

    @NotNull
    private MessageType type;

    @NotBlank
    private String payload;

    protected Message(Queue destination, MessageType type, String payload) {
        this.destination = destination;
        this.type = type;
        this.payload = payload;
    }

    public Queue getDestination() {
        return destination;
    }

    public void setDestination(Queue destination) {
        this.destination = destination;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }

    @Override
    public void validate() {
        ValidatorWrapper.INSTANCE.validate(this);
    }
}
