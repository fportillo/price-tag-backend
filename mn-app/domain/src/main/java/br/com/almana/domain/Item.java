package br.com.almana.domain;

import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * Created by francisco on 11/2/15.
 */
public class Item {

    @EAN
    @NotBlank
    private String ean;

    @Range(min = 1, max = 1000)
    private int qty;


    private Item() {

    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public static Item newInstance(final String pEan, final int pQty) {
        Item item = new Item();
        item.ean = pEan;
        item.qty = pQty;
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (qty != item.qty) return false;
        return ean.equals(item.ean);

    }

    @Override
    public int hashCode() {
        int result = ean.hashCode();
        result = 31 * result + qty;
        return result;
    }
}
