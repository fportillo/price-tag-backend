package br.com.almana.domain;


import com.google.gson.Gson;

import javax.validation.Valid;

/**
 * Created by francisco on 9/18/15.
 */
public class Shop {

    private String name;

    @Valid
    private Location location;

    private Shop() {
    }

    public static Shop newInstance(final double latitude, final double longitude, final String name) {
        final Shop shop = new Shop();
        shop.setLocation(Location.getInstance(latitude, longitude));
        shop.setName(name);
        return shop;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
