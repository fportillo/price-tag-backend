package br.com.almana.exception;

import java.util.function.Supplier;

/**
 * Created by francisco on 11/1/15.
 */
public class UpdateException extends RuntimeException implements Supplier<RuntimeException> {


    public UpdateException(long expectedCount, long actualCount) {
        super(String.format("Expected update count: %d, actual updated count: %d", expectedCount, actualCount));
    }

    @Override
    public RuntimeException get() {
        return this;
    }
}
