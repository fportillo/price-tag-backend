package br.com.almana.util.fixture;

import br.com.almana.domain.Location;

/**
 * Created by francisco on 11/2/15.
 */
public enum LocationFixture {

    ARARAQUARA(Location.getInstance(-22.8815712, -47.0471097));

    private final Location location;

    LocationFixture(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
