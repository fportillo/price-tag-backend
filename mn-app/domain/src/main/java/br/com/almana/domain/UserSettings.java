package br.com.almana.domain;

import br.com.almana.config.AppConfig;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * Class to hold user preferences (or user defined settings).
 * Created by francisco on 10/27/15.
 */
public class UserSettings {

    @NotNull
    @Range(min = 0, max = 100000) // max 100k meters == 100km. Beyond that too many stores would return
    private Integer distanceRange;

    private UserSettings() {

    }

    public static UserSettings getInstance() {
        UserSettings settings = new UserSettings();
        settings.distanceRange = AppConfig.getInstance().getDefaultDistanceRange();
        return settings;
    }

}
