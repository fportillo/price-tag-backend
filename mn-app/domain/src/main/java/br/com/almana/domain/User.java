package br.com.almana.domain;

import br.com.almana.util.HashUtil;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * Representation of the end user of the application.
 * Created by francisco on 10/27/15.
 */
public class User extends BaseEntity {

    public static final String USER_ID_PATH_PARAM = "useruuid";
    public static final String PATH = "/user";
    public static final String API_PATH = String.format("/user/{%s}", USER_ID_PATH_PARAM);
    public static final String MONGO_COLLECTION_NAME = "user";

    @Size(max = 20)
    @Valid
    private Set<ShoppingList> shoppingLists;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String name;

    @NotNull
    private UserSettings settings;

    @NotBlank
    private String salt;

    @NotBlank
    private String passwordHash;

    @NotEmpty
    private List<Role> roles;

    @NotNull
    private Status status;

    private User() {
        this.salt = HashUtil.newSalt();
    }

    public static User newInstance(final String pEmail, String password, List<Role> roles, String fullName) {
        final User user = new User();
        user.shoppingLists = new LinkedHashSet<>();
        user.email = pEmail;
        user.settings = UserSettings.getInstance();
        user.passwordHash = HashUtil.hash(password, user.salt);
        user.name = fullName;
        user.status = Status.WAITING_EMAIL_VERIFICATION;
        user.setRoles(roles);
        return user;
    }

    public enum Status {
        WAITING_EMAIL_VERIFICATION,
        EMAIL_VERIFIED
    }

    public void add(ShoppingList shoppingList) {
        this.shoppingLists.add(shoppingList);
    }

    public Set<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

    public UserSettings getSettings() {
        return settings;
    }

    public String getEmail() {
        return email;
    }

    public String getShoppingListUri(ShoppingList shoppingList) {
        return String.format("%s/%s%s/%s", User.PATH, get_id(), ShoppingList.PATH, shoppingList.getUuid());
    }

    public ShoppingList removeShoppingList(UUID uuid) {
        Optional<ShoppingList> toBeRemoved = shoppingLists.stream().filter((sl) -> sl.getUuid().equals(uuid)).findAny();
        toBeRemoved.ifPresent(shoppingLists::remove);
        return toBeRemoved.orElse(null);
    }

    public void updateShoppingList(ShoppingList shoppingList) {
        final boolean[] found = {false};
        shoppingLists.forEach((sl) -> {
            if (sl.getUuid().equals(shoppingList.getUuid())) {
                sl.setItems(shoppingList.getItems());
                sl.setName(shoppingList.getName());
                found[0] = true;
            }
        });
        if (!found[0]) {
            shoppingLists.add(shoppingList);
        }
    }

    @Override
    public String toString() {
        return toJson();
    }

    @Override
    public void onSave() {
        // do nothing
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
