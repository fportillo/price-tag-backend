package br.com.almana.domain;

import br.com.almana.util.HashUtil;
import com.hazelcast.nio.serialization.DataSerializable;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Pojo that represents
 * Created by francisco on 12/2/15.
 */
public class AuthToken extends BaseEntity {

    public static final String MONGO_COLLECTION_NAME = "authtoken";
    public static final String MAP_KEY = "authToken";
    /*
     * The underline is a reminder that this is the MongoDB string representation of the User's _id (ObjectId).
     */
    @NotBlank
    private String user_id;

    /**
     * The authorization token itself.
     */
    @NotBlank
    private String token;

    @NotNull
    private Date createdAt;

    @NotNull
    @Future
    private Date expiresAt;

    public AuthToken() {
    }

    public AuthToken(String pUser_id, int sessionDurationInMonths) {
        // TODO: use ZonedDateTime
        createdAt = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(createdAt);
        cal.add(Calendar.MONTH, sessionDurationInMonths);
        expiresAt = cal.getTime();
        token = HashUtil.newAuthToken();
        user_id = pUser_id;
    }

    @Override
    public void onSave() {
        // do nothing for now
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public String getToken() {
        return token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthToken authToken = (AuthToken) o;
        return Objects.equals(user_id, authToken.user_id) &&
                Objects.equals(token, authToken.token) &&
                Objects.equals(createdAt, authToken.createdAt) &&
                Objects.equals(expiresAt, authToken.expiresAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user_id, token, createdAt, expiresAt);
    }
}
