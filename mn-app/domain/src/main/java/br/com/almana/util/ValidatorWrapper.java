package br.com.almana.util;

import javax.validation.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by francisco on 10/27/15.
 */
public enum ValidatorWrapper {

    INSTANCE;

    private Validator validator;

    ValidatorWrapper() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public Validator getValidator() {
        return validator;
    }

    public <T> void validate(T aggregate) {
        final Set<ConstraintViolation<T>> violations = validator.validate(aggregate);
        if (!violations.isEmpty()) {
            final List<String> messages = violations.stream()
                    .map((m) -> String.format("%s property %s %s", m.getRootBeanClass().getSimpleName(), m.getPropertyPath().toString(), m.getMessage()))
                    .collect(Collectors.toList());
            throw new ValidationException(messages.toString());
        }
    }
}
