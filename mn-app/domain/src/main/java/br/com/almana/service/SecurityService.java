package br.com.almana.service;

import br.com.almana.config.AppConfig;
import br.com.almana.domain.*;
import br.com.almana.exception.AuthDeniedException;
import br.com.almana.exception.UserNotFoundException;
import br.com.almana.exception.YouShallNotPassException;
import br.com.almana.repository.Repository;
import br.com.almana.util.HashUtil;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.mongodb.BasicDBObject;
import io.micronaut.context.annotation.Value;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@Singleton
public class SecurityService {
    @Inject
    private final Repository<User> userRepository;
    @Inject
    private final Repository<AuthToken> tokenRepository;
    @Inject
    private final Repository<EmailSignUp> emailSignUpRepository;
    @Inject
    private final MessagingService messagingService;

    @Value("${client.session.duration.months:12}")
    private Integer sessionDurationInMonths;

    private static SessionCache sessionCache;

    private static final Logger LOG = LoggerFactory.getLogger(SecurityService.class);
    public static final String AUTH_TOKEN_MAP = "authToken";
    public static final int DEFAULT_SESSION_DURATION_MONTHS = 12;

    public SecurityService() {
        this.userRepository = Repository.getInstance(User.class);
        tokenRepository = Repository.getInstance(AuthToken.class);
        emailSignUpRepository = Repository.getInstance(EmailSignUp.class);

        messagingService = new MessagingService();
        if (sessionCache == null) {
            sessionCache = new SessionCache();
        }
    }

    private class SessionCache {

        private final HazelcastInstance hazelcastInstance;
        private final Map<String, SessionInfo> authTokenCache;

        private SessionCache() {
            Config config = getSessionCacheConfig();

            LOG.info("Calling new HazelCast Instance");
            hazelcastInstance = Hazelcast.newHazelcastInstance(config);
            authTokenCache = hazelcastInstance.getMap(AUTH_TOKEN_MAP);
        }

        public void put(AuthToken authToken, SessionInfo sessionInfo) {
            authTokenCache.put(authToken.getToken(), sessionInfo);
        }

        public void remove(AuthToken authToken) {
            authTokenCache.remove(authToken.getToken());
        }

        public Optional<SessionInfo> get(String authToken) {
            return Optional.ofNullable(authTokenCache.get(authToken));
        }
    }

    Config getSessionCacheConfig() {
        Config config = new Config();
        final MapConfig mapConfig = config.getMapConfig(AUTH_TOKEN_MAP);
        mapConfig.setEvictionPolicy(EvictionPolicy.LRU);
        mapConfig.setMaxIdleSeconds(getSessionCacheMaxIdleSeconds());
        mapConfig.setTimeToLiveSeconds(getSessionTTLSeconds());
        return config;
    }

    protected int getSessionTTLSeconds() {
        // FIXME - REMOVE HARDCODED AND GET THIS FROM CONFIG OBJECT
        return 60;
    }

    protected int getSessionCacheMaxIdleSeconds() {
        // FIXME - REMOVE HARDCODED AND GET THIS FROM CONFIG OBJECT
        return 60;
    }

    public AuthToken authenticate(String email, String password) {
        BasicDBObject query = new BasicDBObject();
        query.append("email", email);
        User user = userRepository.findOneByQuery(query).orElseThrow(new AuthDeniedException());
        if (HashUtil.hash(password, user.getSalt()).equals(user.getPasswordHash())) {
            // first look for an existing token. If not found save a new one.
            Optional<AuthToken> maybeToken = tokenRepository.findOneByQuery(new BasicDBObject("user_id", user.get_id()));
            if (maybeToken.isPresent()) {
                return maybeToken.get();
            }
            AuthToken token = tokenRepository.save(new AuthToken(user.get_id(), getSessionDurationInMonths()));
            SessionInfo info = buildSession(user, token);
            String[] userRoles = user.getRoles().stream().map(Enum::name).toArray((size) -> new String[user.getRoles().size()]);
            info.setUserRoles(userRoles);
            sessionCache.put(token, info);
            return token;
        }
        throw new AuthDeniedException();
    }

    private SessionInfo buildSession(User user, AuthToken token) {
        SessionInfo info = new SessionInfo();
        info.setAuthToken(token);
        info.setUserEmail(user.getEmail());
        info.setUserFullName(user.getName());
        return info;
    }


    public void logout(AuthToken token) {
        sessionCache.remove(token);
        tokenRepository.deleteOne(new BasicDBObject("user_id", token.getUser_id()));
    }

    /**
     * Validates token (a token in the DB is assumed to be valid. Expired tokens are deleted by Mongo :)
     *
     * @param token
     * @return the User associated with the token passed in.
     */
    public SessionInfo authenticate(String token) {
        try {

            // try cache first, if info not available on cache, hit mongo
            final Optional<SessionInfo> optionalSessionInfo = sessionCache.get(token);
            if (optionalSessionInfo.isPresent()) {
                return optionalSessionInfo.get();
            }

            // info not found on cache, hit mongo then
            final AuthToken authToken = tokenRepository.findOneByQuery(new BasicDBObject("token", token)).orElseThrow(AuthDeniedException::new);
            final User user = userRepository.findOneByQuery(new BasicDBObject("_id", new ObjectId(authToken.getUser_id()))).orElseThrow(AuthDeniedException::new);
            return buildSession(user, authToken);

        } catch (NoSuchElementException nsee) {
            throw new AuthDeniedException();
        }
    }

    public void checkUserRoles(String email, List<Role> allowedRoles) {
        User user = userRepository.findOneByQuery(new BasicDBObject("email", email)).get();
        LOG.debug("Roles of {}: {}", user.getEmail(), user.getRoles());
        for (Role userRole : user.getRoles()) {
            if (allowedRoles.contains(userRole)) {
                return;
            }
        }
        throw new YouShallNotPassException(String.format("%s does not have the necessary roles.", email));
    }

    public void signUp(EmailSignUp emailSignUp) {
        LOG.info("New SignUp request received: {}", emailSignUp.getEmail());
        User user = User.newInstance(emailSignUp.getEmail(), emailSignUp.getPassword(), Role.defaultRoles(), emailSignUp.getName());
        userRepository.save(user);
        LOG.debug("Corresponding user {} persisted.", user.getEmail());

        String token = HashUtil.newAuthToken();
        emailSignUp.setConfirmationToken(token);

        emailSignUp.setPassword(user.getPasswordHash());

        emailSignUp.setConfirmationLink(String.format("https://%s/verify?ct=%s", AppConfig.getInstance().getDomain(), token));

        final String emailFrom = String.format("%s@%s", AppConfig.getInstance().getMailFrom(), AppConfig.getInstance().getDomain());
        emailSignUp.setEmailFrom(emailFrom);

        emailSignUpRepository.save(emailSignUp);
        LOG.debug("SignUp request persisted.");


        Message message = new EmailNotification(emailSignUp.toJson());
        try {
            messagingService.sendMessage(message);
        } catch (IOException e) {
            LOG.error("IOException during signup of {}!", user.getEmail(), e);
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            LOG.error("TimeoutException during signup of {}!", user.getEmail(), e);
            throw new RuntimeException(e);
        }
    }

    public void verifyEmail(String confirmationToken) {
        LOG.info("Verifying token {}", confirmationToken);
        BasicDBObject query = new BasicDBObject("confirmationToken", confirmationToken);
        final Optional<EmailSignUp> maybeEmailSignUp = emailSignUpRepository.findOneByQuery(query);
        final String notFoundMessage = String.format("Could not find user associated with ct=%s", confirmationToken);
        if (maybeEmailSignUp.isEmpty()) {
            throw new UserNotFoundException(notFoundMessage);
        }

        final EmailSignUp signUp = maybeEmailSignUp.get();
        BasicDBObject userQuery = new BasicDBObject("email", signUp.getEmail());
        final Optional<User> maybeUser = userRepository.findOneByQuery(userQuery);
        if (maybeUser.isEmpty()) {
            throw new UserNotFoundException(notFoundMessage);
        }
        signUp.burn();
        emailSignUpRepository.replace(signUp);

        final User user = maybeUser.get();
        user.setPasswordHash(signUp.getPassword());
        user.setStatus(User.Status.EMAIL_VERIFIED);
        userRepository.replace(user);

        // TODO: send some kind of email verified/welcome email?
        LOG.info("Token {} successfully verified", confirmationToken);
    }

    public Integer getSessionDurationInMonths() {
        return Optional.ofNullable(sessionDurationInMonths).orElse(DEFAULT_SESSION_DURATION_MONTHS);
    }

    public void setSessionDurationInMonths(Integer sessionDurationInMonths) {
        this.sessionDurationInMonths = sessionDurationInMonths;
    }
}
