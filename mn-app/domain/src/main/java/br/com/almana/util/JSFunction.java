package br.com.almana.util;

/**
 * Created by francisco on 3/8/16.
 */
public enum JSFunction {

    SHOPPING_LIST_MAPPING("shoppingListSearchMappingFunction.js"),
    SHOPPING_LIST_REDUCE("shoppingListSearchReduceFunction.js");

    private static final String CLASSPATH_BASE_DIR = "javascript/";

    private final String value;

    JSFunction(String fileName) {
        this.value = ClassPathUtils.readFileFromClasspath(CLASSPATH_BASE_DIR + fileName);
    }

    public String getValue() {
        return value;
    }
}
