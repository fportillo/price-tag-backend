package br.com.almana.exception;

import java.util.function.Supplier;

/**
 * Created by francisco on 11/1/15.
 */
public class ShoppingListNotFoundException extends RuntimeException implements Supplier<RuntimeException> {

    public ShoppingListNotFoundException(String shoppingListId) {
        super(shoppingListId);
    }

    @Override
    public String getMessage() {
        return String.format("Could not find shoppingList with id = [%s]}", super.getMessage());
    }

    @Override
    public RuntimeException get() {
        return this;
    }
}
