package br.com.almana.domain;

/**
 * Created by francisco on 2/8/16.
 */
public class EmailNotification extends Message {
    public EmailNotification(String payload) {
        super(Queue.EMAIL, MessageType.EMAIL, payload);
    }
}
