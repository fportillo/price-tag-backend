package br.com.almana.domain;

import br.com.almana.util.StringNormalizer;
import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

/**
 * Models a price (product)
 */
public class Product extends BaseEntity {

    public static final String MONGO_COLLECTION_NAME = "product";
    public static final String API_PATH = "/product";
    public static final int DEFAULT_QTY = 1;

    @NotBlank
    private String name;

    @NotBlank
    private String normalizedName;

    @NotBlank
    @EAN
    private String barCode;

    @NotNull
    @DecimalMin("0.00")
    private BigDecimal price;

    @URL
    private String thumbnailUrl;

    @Valid
    private Shop shop;

    @Valid
    @NotNull
    private Location location;

    @NotNull
    @Past
    private Date dateCreated;

    @NotNull
    @Past
    private Date lastUpdate;

    // end user prices are not verified
    // prices published by sellers are verified (not published by mobile apps)
    private boolean verified;

    Product() {
        this.dateCreated = Date.from(Instant.now().minusNanos(1)); // make sure it always passed hibernate validation
        this.lastUpdate = this.dateCreated;
    }

    // real thing
    public static Product newInstance(String barCode, String name, BigDecimal value, Location location, String thumbnailUrl, boolean verified) {
        Product newInstance = new Product();
        newInstance.setBarCode(barCode);
        newInstance.setName(name);
        newInstance.setNormalizedName(StringNormalizer.removeAccents(name));
        newInstance.setPrice(value.setScale(2, BigDecimal.ROUND_HALF_UP));
        newInstance.setLocation(location);
        newInstance.setThumbnailUrl(thumbnailUrl);
        newInstance.setVerified(verified);
        return newInstance;
    }

    // used by tests
    public static Product newInstance(String barCode, String name, BigDecimal value, Shop shop, String thumbnailUrl, boolean verified) {
        Product newInstance = newInstance(barCode, name, value, shop.getLocation(), thumbnailUrl, verified);
        newInstance.setShop(shop);
        return newInstance;
    }


    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.normalizedName = StringNormalizer.removeAccents(this.name);
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public void setUpdated() {
        lastUpdate = new Date();
    }

    public String getNormalizedName() {
        return normalizedName;
    }

    public void setNormalizedName(String normalizedName) {
        this.normalizedName = normalizedName;
    }

    public void updateNormalizedName() {
        this.normalizedName = StringNormalizer.removeAccents(getName());
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return toJson();
    }

    @XmlTransient
    public String getResourceUri() {
        return String.format("%s/%s", Product.API_PATH, get_id());
    }

    @Override
    public void onSave() {
        updateNormalizedName();
        setLastUpdate(Date.from(Instant.now().minusNanos(1)));
    }

    public Item asItem() {
        return Item.newInstance(getBarCode(), DEFAULT_QTY);
    }
}
