package br.com.almana.service;

import br.com.almana.domain.Location;
import br.com.almana.domain.Product;
import br.com.almana.repository.Repository;
import br.com.almana.util.StringNormalizer;
import com.mongodb.BasicDBObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by francisco on 10/13/15.
 */
@Singleton
public class ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    private static final int BARCODE_TOP_RESULTS = 10;

    private final Repository<Product> repository;

    public ProductService() {
        this.repository = Repository.getInstance(Product.class);
    }

    public List<Product> findTopByBarcode(final String barCode) {
        LOGGER.debug("Quering db for products with barCode eq {}", barCode);

        final BasicDBObject filter = new BasicDBObject("barCode", barCode);
        final BasicDBObject sort = new BasicDBObject("verified", -1); // verified true first
        sort.put("price", 1); // then by price

        List<Product> results = repository.findByQuery(filter, Optional.of(sort));

        return results.stream()
                .limit(BARCODE_TOP_RESULTS)
                .collect(Collectors.toList());
    }

    public URI save(Product product) {
        product.updateNormalizedName();
        Product saved = repository.save(product);
        return URI.create(saved.getResourceUri());
    }

    public List<Product> findTopByNameLike(String nameLike) {
        BasicDBObject query = getNameQuery(nameLike);
        return repository.findByQuery(query);
    }

    private BasicDBObject getNameQuery(String nameLike) {
        final Pattern namePattern = getNamePattern(nameLike);
        return new BasicDBObject("normalizedName", namePattern);
    }

    private Pattern getNamePattern(String nameLike) {
        final String normalizedName = StringNormalizer.removeAccents(nameLike);
        return Pattern.compile(String.format("(?i).*%s.*", normalizedName));
    }

    // FIX : refine search
    public List<Product> findByParams(String barCode, Double latitude, Double longitude, String name) {
        // FIXME -> grab from user prefs...
        int maxDistance = 10000; // 10 km
        Optional<Location> location = Optional.empty();
        if (latitude != null && longitude != null) {
            location = Optional.of(Location.getInstance(latitude, longitude));
        }
        if (location.isPresent()) {
            if (StringUtils.isNotBlank(barCode)) {
                return findNearByBarcode(barCode, location.get(), maxDistance);
            }
            if (StringUtils.isNotBlank(name)) {
                return findNearByName(name, location.get(), maxDistance);
            }
        }
        if (StringUtils.isNotBlank(barCode)) {
            return findTopByBarcode(barCode);
        }
        if (StringUtils.isNotBlank(name)) {
            return findTopByNameLike(name);
        }
        return Collections.emptyList();
    }

    public List<Product> findNearByBarcode(String barCode, Location location, int maxDistanceInMeters) {
        BasicDBObject filter = getGeoLocationFilter(location, maxDistanceInMeters);
        filter.put("barCode", barCode);

        // order by here
        BasicDBObject valueAsc = new BasicDBObject();
        valueAsc.put("price", 1);

        return repository.findByQuery(filter, Optional.of(valueAsc));
    }

    private BasicDBObject getGeoLocationFilter(Location location, int maxDistanceInMeters) {
        BasicDBObject filter = new BasicDBObject();

        BasicDBObject near = new BasicDBObject();

        BasicDBObject geometry = new BasicDBObject();
        BasicDBObject geoAttrs = new BasicDBObject();
        geoAttrs.put("type", location.getType());
        geoAttrs.put("coordinates", location.getCoordinates());
        geometry.put("$geometry", geoAttrs);
        geometry.put("$maxDistance", maxDistanceInMeters);

        near.put("$near", geometry);

        filter.put("location", near);
        return filter;
    }

    public List<Product> findNearByName(String name, Location location, int maxDistanceInMeters) {
        BasicDBObject filter = getGeoLocationFilter(location, maxDistanceInMeters);

        filter.put("normalizedName", getNamePattern(name));

        // order by here
        BasicDBObject valueAsc = new BasicDBObject();
        valueAsc.put("price", 1);

        return repository.findByQuery(filter, Optional.of(valueAsc));
    }
}
