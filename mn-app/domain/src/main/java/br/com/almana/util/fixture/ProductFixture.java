package br.com.almana.util.fixture;

import br.com.almana.domain.Product;

import java.math.BigDecimal;

/**
 * Test data prices
 * Created by francisco on 10/13/15.
 */
public enum ProductFixture {

    LEITE_ITALAC_DALBEN_FROM_USER(Product.newInstance("7898080640611", "Leite Italac Integral", new BigDecimal("2.59"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    LEITE_ITALAC_SAMS_CLUB(Product.newInstance("7898080640611", "Leite Italac Integral", new BigDecimal("2.31"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, true)),
    LEITE_ITALAC_PAO_DE_ACUCAR(Product.newInstance("7898080640611", "Leite Italac Integral", new BigDecimal("3.26"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), null, true)),
    LEITE_ITALAC_PAULISTAO_FROM_USER(Product.newInstance("7898080640611", "Leite Italac Integral", new BigDecimal("1.59"), ShopFixture.PAULISTAO_OROZIMBO_MAIA.getShop(), null, false)),

    NUTELLA(Product.newInstance("7898024396529", "Nutella Ferrero", new BigDecimal("15.99"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, false)),
    NESCAUZINHO(Product.newInstance("7891000379103", "Nescau prontinho", new BigDecimal("1.99"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, false)),
    DIET_WAY(Product.newInstance("7896730900177", "Diet way", new BigDecimal("17.99"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), null, false)),
    PAO_DE_MEL_LAMBERTZ(Product.newInstance("4006894289800", "Lambertz - Pão de Mel HSB Lebkuchen com Chocolate 500g", new BigDecimal("13.98"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "http://www.stuttgart.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/l/a/lambertz_-_schokoladen_lebkuchen_herzen_sterne_brezeln_500g.jpg", true)),
    TODDYNHO(Product.newInstance("7894321722016", "Toddynho Sabor Chocolate", new BigDecimal("1.60"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "http://araujo.vteximg.com.br/arquivos/ids/2788599-200-200/7894321722016_1img-imagem-id-21258.jpg", true)),
    MINI_BOLO_CHOCOLATE(Product.newInstance("7896002364553", "Mini Bolo de Chocolate Pullman 40g", new BigDecimal("1.49"), ShopFixture.DALBEN_TAQUARAL.getShop(), "http://mambo.vteximg.com.br/arquivos/ids/166284-292-292/7896002364553_minibolo_pullman_chocolate_40g_01.jpg", true)),
    PINGUINOS_ANA_MARIA(Product.newInstance("7896002369787", "Pingüinos - Ana Maria - 80g", new BigDecimal("2.19"), ShopFixture.DALBEN_TAQUARAL.getShop(), "http://static.openfoodfacts.org/images/products/789/600/236/9787/front.9.200.jpg", true)),
    ANA_MARIA_CHOCOLATE(Product.newInstance("7896002362436", "Bolinho Ana Maria Chocolate 80g", new BigDecimal("1.99"), ShopFixture.DALBEN_TAQUARAL.getShop(), "http://www.superprix.com.br/content/images/thumbs/0008240_bolinho-ana-maria-chocolate-80g_300.png", false)),
    LOCAO_POS_BARBA_BOZZANO(Product.newInstance("7891350032277", "Loção pós barba bozzano Polaris 100ml", new BigDecimal("15.99"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    LOCAO_POS_BARBA_BIC(Product.newInstance("0070330770188", "Loção pós barba BIC bálsamo sensitive 90ml", new BigDecimal("14.99"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    LHOMME_AFTER_SHAVE_LOCAO_POS_BARBA(Product.newInstance("7891177039169", "L'Homme After Shave Loção pós barba 100ml", new BigDecimal("17.90"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),

    // products for shopping list
    SABONETE_NIVEA_LEITE_DALBEN_TAQUARAL(Product.newInstance("4005808806430", "Sabonete com hidratande Nívea - Leite 90g", new BigDecimal("1.16"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    SABONETE_NIVEA_LEITE_SAMS_CLUB(Product.newInstance("4005808806430", "Sabonete com hidratande Nívea - Leite 90g", new BigDecimal("0.98"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, false)),
    SABONETE_NIVEA_LEITE_PAO_DE_ACUCAR(Product.newInstance("4005808806430", "Sabonete com hidratande Nívea - Leite 90g", new BigDecimal("2.15"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), null, false)),

    //http://mambo.vteximg.com.br/arquivos/ids/167793-150-150/7896098902400_amaciante_ype_01.jpg
    AMACIENTE_IPE_INTENSO_DALBEN(Product.newInstance("7896098903032", "Amaciente Ipê intenso 2l", new BigDecimal("9.90"), ShopFixture.DALBEN_TAQUARAL.getShop(), "http://mambo.vteximg.com.br/arquivos/ids/167793-150-150/7896098902400_amaciante_ype_01.jpg", false)),
    AMACIENTE_IPE_INTENSO_SAMS_CLUB(Product.newInstance("7896098903032", "Amaciente Ipê intenso 2l", new BigDecimal("7.80"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "http://mambo.vteximg.com.br/arquivos/ids/167793-150-150/7896098902400_amaciante_ype_01.jpg", false)),
    AMACIENTE_IPE_INTENSO_PAO_DE_ACUCAR(Product.newInstance("7896098903032", "Amaciente Ipê intenso 2l", new BigDecimal("14.90"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), "http://mambo.vteximg.com.br/arquivos/ids/167793-150-150/7896098902400_amaciante_ype_01.jpg", false)),

    DETERGENTE_LIMPOL_CRISTAL_DALBEN(Product.newInstance("7891022100372", "Detergente Limpol Cristal 500ml", new BigDecimal("1.19"), ShopFixture.DALBEN_TAQUARAL.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products/limpol-cristal-500ml-liquido_300x300-PU61fdc_1.jpg", false)),
    DETERGENTE_LIMPOL_CRISTAL_SAMS_CLUB(Product.newInstance("7891022100372", "Detergente Limpol Cristal 500ml", new BigDecimal("1.65"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products/limpol-cristal-500ml-liquido_300x300-PU61fdc_1.jpg", false)),
    DETERGENTE_LIMPOL_CRISTAL_PAO_DE_ACUCAR(Product.newInstance("7891022100372", "Detergente Limpol Cristal 500ml", new BigDecimal("1.89"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products/limpol-cristal-500ml-liquido_300x300-PU61fdc_1.jpg", false)),

    SABAO_LIQUIDO_ARIEL_DALBEN(Product.newInstance("7501006705928", "Ariel Líquido Solução Total 1 litro", new BigDecimal("8.87"), ShopFixture.DALBEN_TAQUARAL.getShop(), "http://img.submarino.com.br/produtos/01/00/item/112774/0/112774060_1GG.jpg", false)),
    SABAO_LIQUIDO_ARIEL_SAMS_CLUB(Product.newInstance("7501006705928", "Ariel Líquido Solução Total 1 litro", new BigDecimal("9.90"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "http://img.submarino.com.br/produtos/01/00/item/112774/0/112774060_1GG.jpg", false)),
    SABAO_LIQUIDO_ARIEL_PAO_DE_ACUCAR(Product.newInstance("7501006705928", "Ariel Líquido Solução Total 1 litro", new BigDecimal("10.15"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), "http://img.submarino.com.br/produtos/01/00/item/112774/0/112774060_1GG.jpg", false)),

    FILTRO_PAPEL_JOVITA_DALBEN(Product.newInstance("7891021002165", "Filtro Papel Jovita 103", new BigDecimal("4.50"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    FILTRO_PAPEL_JOVITA_SAMS_CLUB(Product.newInstance("7891021002165", "Filtro Papel Jovita 103", new BigDecimal("1.50"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, false)),
    FILTRO_PAPEL_JOVITA_PAO_DE_ACUCAR(Product.newInstance("7891021002165", "Filtro Papel Jovita 103", new BigDecimal("5.30"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), null, false)),

    MARGARINA_DELICIA_SUPREME_DALBEN(Product.newInstance("7891080112836", "Margarina com Sal Delícia Supreme 500g", new BigDecimal("3.49"), ShopFixture.DALBEN_TAQUARAL.getShop(), null, false)),
    MARGARINA_DELICIA_SAMS_CLUB(Product.newInstance("7891080112836", "Margarina com Sal Delícia Supreme 500g", new BigDecimal("3.49"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), null, false)),
    MARGARINA_DELICIA_PAO_DE_ACUCAR(Product.newInstance("7891080112836", "Margarina com Sal Delícia Supreme 500g", new BigDecimal("3.49"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), null, false)),

    LASANHA_BOLONHESA_SADIA_DALBEN(Product.newInstance("7893000439825", "Lasanha Bolonhesa Sadia", new BigDecimal("12.90"), ShopFixture.DALBEN_TAQUARAL.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products_image/7893000439825", false)),
    LASANHA_BOLONHESA_SADIA_SAMS_CLUB(Product.newInstance("7893000439825", "Margarina com Sal Delícia Supreme 500g", new BigDecimal("10.30"), ShopFixture.SAMS_CLUB_CAMPINAS.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products_image/7893000439825", false)),
    LASANHA_BOLONHESA_SADIA_PAO_DE_ACUCAR(Product.newInstance("7893000439825", "Margarina com Sal Delícia Supreme 500g", new BigDecimal("0.97"), ShopFixture.PAO_DE_ACUCAR_ALBERTO_SARMENTO.getShop(), "https://s3.amazonaws.com/bluesoft-cosmos/products_image/7893000439825", false));



    private final Product product;

    ProductFixture(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }
}
