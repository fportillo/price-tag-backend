package br.com.almana.service;

import br.com.almana.config.AppConfig;
import br.com.almana.domain.Message;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by francisco on 2/7/16.
 */
@Singleton
public class MessagingService {

    private static final Logger LOG = LoggerFactory.getLogger(MessagingService.class);
    public static final String NO_EXCHANGE = "";

    private ConnectionFactory connectionFactory;

    public MessagingService() {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(AppConfig.getInstance().getRabbitHost());
    }

    public void sendMessage(Message message) throws IOException, TimeoutException {
        LOG.debug("Connecting to rabbitmq...");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        LOG.debug("Connection to rabbitmq and creation of channel is successful.");

        final String queueName = message.getDestination().name();

        channel.queueDeclare(queueName, true, false, false, null);

        LOG.info("Sending message: {}", message.toJson());
        channel.basicPublish(NO_EXCHANGE, queueName, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getPayload().getBytes());
        LOG.info("Message sent: {}", message.toJson());

        channel.close();
        LOG.debug("RabbitMQ Channel closed.");

        connection.close();
        LOG.debug("RabbitMQ connection closed.");
    }


}
