package br.com.almana.domain;

/**
 * Created by laercio on 07/11/15.
 */
public interface Jsonifiable {

    /**
     * Provides a String JSON representation of itself.
     * @return
     */
    String toJson();
}
