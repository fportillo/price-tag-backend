package br.com.almana.repository;

import br.com.almana.config.AppConfig;
import br.com.almana.domain.BaseEntity;
import br.com.almana.exception.UpdateException;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

/**
 * Generic repository class. Responsible from grabbing data from underlying database.
 * Created by francisco on 9/28/15.
 */
public class Repository<T extends BaseEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Repository.class);

    private final Class<T> type;
    private final int limit;
    private final MongoCollection<Document> collection;

    private Repository(Class<T> type) {
        this.type = type;
        this.limit = AppConfig.getInstance().getPageSize();
        this.collection = Mongo.INSTANCE.collectionByType(type);
    }

    public static <T extends BaseEntity> Repository<T> getInstance(Class<T> type) {
        return new Repository<>(type);
    }

    public T save(final T aggregate) {
        aggregate.onSave();
        if (StringUtils.isNotBlank(aggregate.get_id())) {
            final String errorMsg = String.format("Cannot insert existing document. Collection: %s, _id: %s", type.getSimpleName(), aggregate.get_id());
            LOGGER.error(errorMsg);
            throw new IllegalStateException(errorMsg);
        }
        aggregate.validate();
        final Document document = Document.parse(new Gson().toJson(aggregate));
        getCollection().insertOne(document);
        final ObjectId updatedId = (ObjectId) document.get("_id");
        aggregate.set_id(updatedId.toHexString());
        return aggregate;
    }

    public void deleteOne(final BasicDBObject query) {
        getCollection().deleteOne(query);
    }

    public void deleteMany(final BasicDBObject query) {
        getCollection().deleteMany(query);
    }

    public T update(final T aggregate, BasicDBObject updateSection) {
        aggregate.validate();
        final UpdateResult updateResult = getCollection().updateOne(eq("_id", new ObjectId(aggregate.get_id())), new BasicDBObject("$set", updateSection));
        final long modifiedCount = updateResult.getModifiedCount();
        if (modifiedCount > 1) {
            throw new UpdateException(1, modifiedCount);
        }
        return aggregate;
    }


    public Optional<T> get(final ObjectId _id) {
        final Document document = getCollection().find(eq("_id", _id)).first();
        return getT(document);
    }

    private Optional<T> getT(Document document) {
        Optional<T> optional = Optional.empty();
        if (document != null) {
            final T value = fixObjectId(document);
            optional = Optional.of(value);
        }
        return optional;
    }

    private T fixObjectId(Document document) {
        /*
         * FIXME: make an interface String toJson() and BasicDBObject fromJson(String) for the domain interfaces.
         */
        ObjectId _id = document.getObjectId("_id");
        document.put("_id", null);
        final T value = new Gson().fromJson(document.toJson(), type);
        // gson makes a mess converting ObjectId to hex String and vice-versa, so overriding
        value.set_id(_id.toHexString());
        return value;
    }

    public List<T> findByQuery(final BasicDBObject query, Optional<Bson> sortBy) {
        if (sortBy.isPresent()) {
            return toResult(getCollection().find(query).sort(sortBy.get()).limit(limit));
        }
        return findByQuery(query);
    }

    public List<T> findByQuery(final BasicDBObject query) {
        return toResult(getCollection().find(query).limit(limit));
    }

    public Optional<T> findOneByQuery(final BasicDBObject query) {
        return toResult(getCollection().find(query).first());
    }

    public List<T> findByQuery(final Bson query) {
        LOGGER.debug("Executing query with max results limited to {}", limit);
        return toResult(getCollection().find(query).limit(limit));
    }

    private List<T> toResult(FindIterable<Document> documents) {
        final List<T> results = new ArrayList<>();
        for (Document doc : documents) {
            final T value = fixObjectId(doc);
            results.add(value);
        }
        LOGGER.debug("{} results returned", results.size());
        return results;
    }

    private Optional<T> toResult(Document doc) {
        return getT(doc);
    }

    public T findFirst() {
        final Document first = getCollection().find().first();
        return toResult(first).get();
    }

    private MongoCollection<Document> getCollection() {
        return collection;
    }

    public T replace(T aggregate) {
        aggregate.onSave();
        Document aggregateWithoutId = Document.parse(aggregate.toJson());
        aggregateWithoutId.remove("_id");
        UpdateResult id = getCollection().replaceOne(new Document("_id", new ObjectId(aggregate.get_id())), aggregateWithoutId);
        if (id.getModifiedCount() > 0) {
            LOGGER.debug("Aggregate _id {} has been replaced with {}", aggregate.get_id(), aggregate);
        }
        return aggregate;
    }
}
