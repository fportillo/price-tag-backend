package br.com.almana.util.fixture;

import br.com.almana.domain.Role;
import br.com.almana.domain.User;

import java.util.Collections;

/**
 * Fixture to create dev/test bootstrap data.
 * Created by francisco on 11/1/15.
 */
public enum UserFixture {

    JOHN_DOE(User.newInstance("john.doe@gmail.com", "foobar", Collections.singletonList(Role.USER), "John Doe"));

    private final User user;

    UserFixture(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
