package br.com.almana.util.fixture;

import br.com.almana.domain.Shop;

/**
 * Fixture for shops
 * Created by francisco on 10/13/15.
 */
public enum ShopFixture {

    DALBEN_TAQUARAL(Shop.newInstance(-22.8785233, -47.0464989, "Dalben Taquaral")),
    DROGASIL_TAQUARAL(Shop.newInstance(-22.8781434, -47.0464572, "Drogasil Taquaral")),
    SAMS_CLUB_CAMPINAS(Shop.newInstance(-22.8608501, -47.0259808, "Sam's Club - Campinas")),
    PAO_DE_ACUCAR_ALBERTO_SARMENTO(Shop.newInstance(-22.9550835, -47.2316457, "Pao de acucar alberto sarmento")),
    PAULISTAO_OROZIMBO_MAIA(Shop.newInstance(-22.8800959, -47.069972, "Paulistao Orozimbo Maia"));

    public static final double CAMPINAS_ELEVATION = 685; // Campinas's elevation in meters

    private Shop shop;

    ShopFixture(Shop shop) {
        this.shop = shop;
    }

    public Shop getShop() {
        return shop;
    }
}
