package br.com.almana.email.templates;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Created by francisco on 2/8/16.
 */
public class TemplateEngineTest {

    private static final String EXPECTED = "" +
            "<html>\n" +
            "    <head>\n" +
            "        <title>Confirmação de email</title>\n" +
            "    </head>\n" +
            "    <body>\n" +
            "        <h1>Bem vindo</h1>\n" +
            "        <p>\n" +
            "            Para completar seu registro, é necessário que você confirme sua identidade.\n" +
            "        </p>\n" +
            "        Clique nesse link para confirmar seu email:\n" +
            "        <a href=\"https://dummydomain.com/verify?ct=fakect\">https://dummydomain.com/verify?ct=fakect</a>\n" +
            "    </body>\n" +
            "</html>";

    @Test
    public void testSignUpEmailTemplate() {
        TemplateEngine engine = new TemplateEngine();

        Map<String, Object> model = new HashMap<>();
        model.put("title", "Confirmação de email");
        model.put("welcomeHeader", "Bem vindo");
        model.put("welcomeText", "Para completar seu registro, é necessário que você confirme sua identidade.");
        model.put("clickHereText", "Clique nesse link para confirmar seu email:");
        model.put("link", "https://dummydomain.com/verify?ct=fakect");
        assertEquals(EXPECTED, engine.process(AppTemplates.SIGN_UP_EMAIL, model));
    }

}