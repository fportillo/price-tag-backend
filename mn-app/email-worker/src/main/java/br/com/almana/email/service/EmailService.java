package br.com.almana.email.service;

import br.com.almana.config.AppPropertiesLoader;
import br.com.almana.email.domain.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by francisco on 2/8/16.
 */
public class EmailService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailService.class);

    public void send(Email email) throws MessagingException {
        email.validate();

        Properties properties = AppPropertiesLoader.load("local.properties");

        Session session = Session.getDefaultInstance(properties);

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(email.getFrom()));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email.getTo()));

            message.setSubject(email.getSubject());

            message.setContent(email.getContent(), "text/html");

            LOG.debug("OK, about to send the email message {} ", message);
            Transport.send(message);

            LOG.info("Successfully sent email to {}", email.getTo());
    }


}
