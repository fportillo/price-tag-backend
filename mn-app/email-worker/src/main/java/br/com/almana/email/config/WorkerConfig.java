package br.com.almana.email.config;

import br.com.almana.config.AppPropertiesLoader;
import br.com.almana.util.ValidatorWrapper;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Properties;

/**
 * Created by francisco on 2/8/16.
 */
public class WorkerConfig {

    private static WorkerConfig instance;

    private final Properties properties;

    @NotBlank
    private String smtpHost;

    @NotNull
    @Range(min = 0, max = 65536)
    private Integer smtpPort;

    @NotBlank
    private final String rabbitHost;

    private WorkerConfig() {
        properties = AppPropertiesLoader.load("local.properties");

        this.smtpHost = properties.getProperty("mail.smtp.host");

        this.smtpPort = Integer.valueOf(properties.getProperty("mail.smtp.port"));

        this.rabbitHost = properties.getProperty("rabbitHost");

        ValidatorWrapper.INSTANCE.validate(this);
    }

    public static WorkerConfig getInstance() {
        if (instance == null) {
            instance = new WorkerConfig();
        }
        return instance;
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public Integer getSmtpPort() {
        return smtpPort;
    }

    public Properties getProperties() {
        return properties;
    }

    public String getRabbitHost() {
        return rabbitHost;
    }

}
