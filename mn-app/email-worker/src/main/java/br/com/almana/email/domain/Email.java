package br.com.almana.email.domain;

import br.com.almana.domain.HibValidatable;
import br.com.almana.util.ValidatorWrapper;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by francisco on 2/8/16.
 */
public class Email implements HibValidatable {

    @org.hibernate.validator.constraints.Email
    private String from;

    @org.hibernate.validator.constraints.Email
    private String to;

    @NotBlank
    private String subject;

    @NotBlank
    private String content;

    public Email(String content, String from, String subject, String to) {
        this.content = content;
        this.from = from;
        this.subject = subject;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    @Override
    public void validate() {
        ValidatorWrapper.INSTANCE.validate(this);
    }
}
