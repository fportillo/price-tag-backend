package br.com.almana;

import br.com.almana.config.AppPropertiesLoader;
import br.com.almana.domain.EmailSignUp;
import br.com.almana.email.domain.Email;
import br.com.almana.email.templates.AppTemplates;
import br.com.almana.email.templates.TemplateEngine;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by francisco on 2/8/16.
 */
public class EmailProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(EmailProcessor.class);

    private final String message;
    private final TemplateEngine engine;

    public EmailProcessor(String message) {
        this.message = message;
        this.engine = new TemplateEngine();
    }

    /**
     * Actually sends an email message.
     */
    public Email process() {
        final EmailSignUp signUp = new Gson().fromJson(message, EmailSignUp.class);

        // FIXME: all of this is "type-coded" - refactor to OO
        Properties properties = AppPropertiesLoader.load("sign-up.properties");
        Map<String, Object> model = new HashMap<>();
        final String title = properties.getProperty("signUpEmail.title");
        model.put("title", title);
        model.put("welcomeHeader", properties.getProperty("signUpEmail.welcomeHeader"));
        model.put("welcomeText", properties.getProperty("signUpEmail.welcomeText"));
        model.put("clickHereText", properties.getProperty("signUpEmail.clickHereText"));
        model.put("link", signUp.getConfirmationLink());

        final String content = engine.process(AppTemplates.SIGN_UP_EMAIL, model);

        return new Email(content, signUp.getEmailFrom(), title, signUp.getEmail());
    }
}
