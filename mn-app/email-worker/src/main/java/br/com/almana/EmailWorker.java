package br.com.almana;

import br.com.almana.domain.Queue;
import br.com.almana.email.config.WorkerConfig;
import br.com.almana.email.domain.Email;
import br.com.almana.email.service.EmailService;
import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * Rabbitmq consumer for email messages.
 * Created by francisco on 2/8/16.
 */
public class EmailWorker {

    private static final Logger LOG = LoggerFactory.getLogger(EmailWorker.class);

    private static final EmailService EMAIL_SERVICE = new EmailService();
    private static Channel channel;
    private static boolean started = false;

    public static void main(String[] args) throws IOException, TimeoutException {
        channel = start();
        started = true;
    }

    public static Channel start() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(WorkerConfig.getInstance().getRabbitHost());
        final Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        final Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String jsonMessage = new String(body, StandardCharsets.UTF_8);
                try {
                    LOG.info("Received message: {}", jsonMessage);

                    final Email email = new EmailProcessor(jsonMessage).process();
                    EMAIL_SERVICE.send(email);
                    LOG.info("Processing of {} completed. About to ack...", jsonMessage);
                    channel.basicAck(envelope.getDeliveryTag(), false);
                    LOG.info("Ack done.");
                } catch (MessagingException e) {
                    LOG.error("Error while consuming email: {}", jsonMessage, e);
                }

            }
        };

        channel.basicConsume(Queue.EMAIL.name(), false, consumer);
        LOG.info(" [*] Waiting for messages. To kill email-worker press CTRL+C");
        return channel;
    }

    public static void stop() throws IOException, TimeoutException {
        if (channel != null & started) {
            channel.close();
        }
    }

}
