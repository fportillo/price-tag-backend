package br.com.almana.email.templates;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Created by francisco on 2/8/16.
 */
public class TemplateEngine {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateEngine.class);

    private final Configuration configuration;

    public TemplateEngine() {
        configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setClassLoaderForTemplateLoading(getClass().getClassLoader(), "/");
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.displayName());
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
    }

    public String process(AppTemplates signUpEmail, Map<String, Object> model) {
        try {
            Template template = configuration.getTemplate(signUpEmail.getTemplateName());
            StringWriter sw = new StringWriter();
            template.process(model, sw);
            return sw.toString();
        } catch (IOException | TemplateException e) {
            LOG.error("Error rendering template: {}", signUpEmail.getTemplateName(), e);
            throw new RuntimeException(e);
        }
    }

}
