package br.com.almana.email.templates;

/**
 * Created by francisco on 2/8/16.
 */
public enum AppTemplates {

    SIGN_UP_EMAIL("sign-up.ftl");

    private final String templateName;

    AppTemplates(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }
}
