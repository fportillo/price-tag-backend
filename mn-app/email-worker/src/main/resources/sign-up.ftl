<html>
    <head>
        <title>${title}</title>
    </head>
    <body>
        <h1>${welcomeHeader}</h1>
        <p>
            ${welcomeText}
        </p>
        ${clickHereText}
        <a href="${link}">${link}</a>
    </body>
</html>