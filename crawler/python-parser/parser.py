from abc import ABC
from html.parser import HTMLParser
from html.entities import name2codepoint
import json


class PriceTagProduct(ABC):
    def __init__(self) -> None:
        super().__init__()
        # TODO: from product_raw transform to price tag internal model


class MyHTMLParser(HTMLParser, ABC):

    def __init__(self, *, convert_charrefs=True):
        super().__init__(convert_charrefs=convert_charrefs)
        self.is_script = False
        self.product_raw = None

    def handle_starttag(self, tag, attrs):
        print("Start tag:", tag)
        if tag == 'script':
            self.is_script = True
        for attr in attrs:
            print("     attr:", attr)

    def handle_endtag(self, tag):
        print("End tag  :", tag)
        self.is_script = False

    def handle_data(self, data):
        if self.is_script:
            try:
                maybe_a_product = json.loads(data)
                if maybe_a_product['@type'] == "Product":
                    self.product_raw = maybe_a_product
            except Exception as e:
                pass
        print("Data     :", data)

    def handle_comment(self, data):
        print("Comment  :", data)

    def handle_entityref(self, name):
        c = chr(name2codepoint[name])
        print("Named ent:", c)

    def handle_charref(self, name):
        if name.startswith('x'):
            c = chr(int(name[1:], 16))
        else:
            c = chr(int(name))
        print("Num ent  :", c)

    def handle_decl(self, data):
        print("Decl     :", data)


filename = '5465eaa670a4afc0ac7edb46d9cb62c5_coca-cola-zero-peach.html'
parser = MyHTMLParser()

with open(filename, 'r') as html_file:
    html_content = ''
    for line in html_file.readlines():
        html_content += line + '\n'

    parser.feed(html_content)
    print(json.dumps(parser.product_raw, sort_keys=True, indent=4))
