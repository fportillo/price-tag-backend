# Pricetag Backend

This is an old personal project and currently "left-aside" backend/java project for a mobile app called pricetag.

## Installation

### Pre requirements

#### Java 14+
Make sure you have java 14+ running:

```
[14:32:12] % javac -version
javac 14.0.1
```

#### Maven 3.6.2+
And also maven 3.6.2+:

```
[14:45:10] % mvn -version
Apache Maven 3.6.2 (40f52333136460af0dc0d7232c0dc0bcf0d9e117; 2019-08-27T17:06:16+02:00)
```

#### docker + docker-compose
You'll need docker compose to build the project.


### Building the project
Bring mongo-db + rabbit-mq up:

```
$ cd mn-app && docker-compose up
```

Open another terminal window and check the services (mongo and rabbit-mq) are up and running:
```
[14:50:42] % docker-compose ps
            Name                          Command               State                                             Ports
------------------------------------------------------------------------------------------------------------------------------------------------------------------
mn-app_mongodb_1               docker-entrypoint.sh mongod      Up      0.0.0.0:27017->27017/tcp
mn-app_rabbitmq-management_1   docker-entrypoint.sh rabbi ...   Up      15671/tcp, 0.0.0.0:15672->15672/tcp, 25672/tcp, 4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp
```

Then you can build the project with maven:

```
$ mvn clean install
```
* Note: if either mongo-db or rabbit-mq are not available as above, the build may fail.

### Bring the application up

If you were able to build the project, now you can try to bring the web-api up:

```
(virtual-env) ➜  mn-app git:(master) ✗
[14:53:00] % java -jar web-api/target/web-api-1.0-SNAPSHOT.jar
```

* Note: there will be stacktraces and errors in the app logs after startup. This is because
the migration to MN was never completed.

You can test if the security filter is working:

```
[14:57:10] % curl -i localhost:8080/helloworld
HTTP/1.1 401 Unauthorized
transfer-encoding: chunked
connection: close
```

The 401 Unauthorized is expected.


## About Pricetag

The idea of pricetag was to use the camera on your mobile phone to scan barcodes and perform
API calls to share product prices per barcode and also to compare product prices across different grocery stores.
Users could add their shopping lists and the app would calculate where it would be the cheapest
to grocery store to buy from. 

## Project strucutre

### crawler

A POC for parsing raw HTML files from the output of web crawlers. There's a python file sample checked in.

### mn-app

MN stands for Micronaut (https://micronaut.io/). There was a recent attempt to modernize 
this backend from the "old" JAX-RS 2.0 functional version to the newest java microframework.
The project started with a programatic approach, bringing up a Jetty Servlet container 
very close to what spring boot does today, but without spring. 
The backend at the current state is not functional, as the migration to micronaut was 
never completed: actually it was more an attempt to access the MN maturity than making 
progress with the original project anyways.

### mn-app structure

The mn-app is composed of 3 modules:

#### domain 
Where the core POC classes are, with the domain logic and models.

#### email-worker 
An independent process that subscriber to a rabbit-mq email queue 
and sends the related email everytime a message is consumed from the queue.

#### web-api
The module where the REST/HTTP endpoints are located. There are endpoints
such as login, sign up, email verification and also endpoints to store and read 
products and shoppinglists. Note: the business model builds uppon information sharing,
so one product/list is not tied to the user. All users benefit from the same pool of
information.

